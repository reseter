VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cTimer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
'   Module          : cTimer
'   Description     : Hi-Performance Timer Class
'   Author          : C. Eswar Santhosh
'   Last Updated    : 08th February, 2000.
'   Notes           : If the System does not support a Hi-Resolution timer, uses the very approximate
'                     GetTickCount function. This class is used as a Stop Watch to measure elapsed time
'                     between StartTimer() and EndTimer() Calls.
'
'   Copyright Info  :
'
'   This Class module is provided AS-IS. This Class module can be used as a part of a compiled
'   executable whether freeware or not. This Class module may not be posted to any web site
'   or BBS or any redistributable media like CD-ROM without the consent of the author.
'
'   e-mail   : eswar_santhosh@yahoo.com
'
'   Revision History :
'
'
'
'-------------------------------------------------------------------------------------------------------------
Option Explicit
'
' API Declarations
'
Private Declare Function QueryPerformanceCounter _
                Lib "kernel32" (lpPerformanceCount As Currency) As Long
Private Declare Function QueryPerformanceFrequency _
                Lib "kernel32" (lpFrequency As Currency) As Long
Private Declare Function GetTickCount _
                Lib "kernel32" () As Long
'
' Local Variables
'
Dim timStart As Currency    ' The Starting Counter of the Timer
Dim timElapsed As Double    ' No. of Elapsed Milliseconds
Dim nFrequency As Currency  ' Frequency of the Hi-Resolution timer if it is avilable, else zero
Dim bRunning As Boolean     ' Is the Timer running ?

Public Sub EndTimer()
    '
    ' Ends the Counter
    '
    '<EhHeader>
    On Error GoTo EndTimer_Err
    '</EhHeader>
    timElapsed = GetElapsedTime     ' Store this Value
    bRunning = False
    '<EhFooter>
    Exit Sub
EndTimer_Err:
    Controlar_Error Erl, Err.Description, "Reseter.cTimer.EndTimer"
    Resume Next
    '</EhFooter>
End Sub

Public Sub StartTimer()
Attribute StartTimer.VB_Description = "Starts the Timer"
    '
    ' Starts the Counter
    '
    '<EhHeader>
    On Error GoTo StartTimer_Err
    '</EhHeader>
    timElapsed = 0      ' Reset Counter
    bRunning = True

    If nFrequency <> 0 Then
        QueryPerformanceCounter timStart
    Else
        timStart = GetTickCount
    End If

    '<EhFooter>
    Exit Sub
StartTimer_Err:
    Controlar_Error Erl, Err.Description, "Reseter.cTimer.StartTimer"
    Resume Next
    '</EhFooter>
End Sub

Private Function GetElapsedTime() As Double
    '
    ' Calculates the Elapsed Time
    '
    '<EhHeader>
    On Error GoTo GetElapsedTime_Err
    '</EhHeader>
    Dim timEnd As Currency

    If nFrequency <> 0 Then
        QueryPerformanceCounter timEnd
        GetElapsedTime = ((timEnd - timStart) / nFrequency) * 1000
    Else
        timEnd = GetTickCount
        GetElapsedTime = timEnd - timStart
    End If

    '<EhFooter>
    Exit Function
GetElapsedTime_Err:
    Controlar_Error Erl, Err.Description, "Reseter.cTimer.GetElapsedTime"
    Resume Next
    '</EhFooter>
End Function

Private Sub Class_Initialize()
    '
    ' Check if Hi-resolution timer is available
    '
    '<EhHeader>
    On Error GoTo Class_Initialize_Err
    '</EhHeader>
    Dim APIRetVal As Long
    APIRetVal = QueryPerformanceFrequency(nFrequency)

    If APIRetVal = 0 Then
        nFrequency = 0          ' Zero indicates that the ordinary GetTickCount function must be used
    End If

    '<EhFooter>
    Exit Sub
Class_Initialize_Err:
    Controlar_Error Erl, Err.Description, "Reseter.cTimer.Class_Initialize"
    Resume Next
    '</EhFooter>
End Sub

Public Property Get Elapsed() As Double
Attribute Elapsed.VB_Description = "Returns Elapsed Time in MilliSeconds. If the Timer is running, returns Zero."
    '
    ' Returns Elapsed time
    '
    '<EhHeader>
    On Error GoTo Elapsed_Err

    '</EhHeader>
    If bRunning Then
        Elapsed = GetElapsedTime    ' Return Intermediate Time
    Else
        Elapsed = timElapsed        ' Return the Last counter's timing
    End If

    '<EhFooter>
    Exit Property
Elapsed_Err:
    Controlar_Error Erl, Err.Description, "Reseter.cTimer.Elapsed"
    Resume Next
    '</EhFooter>
End Property
'Public Property Get Frecuencia() As Currency
'        '
'        ' Regresa la frecuencia del timer
'        '
'        '<EhHeader>
'        On Error GoTo Frecuencia_Err
'        '</EhHeader>
'100     Frecuencia = nFrequency
'        '<EhFooter>
'        Exit Property
'
'Frecuencia_Err:
'
'        Err.Raise vbObjectError + 100, "Reseter.cTimer.Frecuencia", "cTimer component failure"
'        '</EhFooter>
'End Property
'Public Property Get IsHiTimer() As Boolean
'        '
'        ' Returns True if a Hi-Performance timer is used
'        '
'        '<EhHeader>
'        On Error GoTo IsHiTimer_Err
'        '</EhHeader>
'100     IsHiTimer = (nFrequency <> 0)
'        '<EhFooter>
'        Exit Property
'
'IsHiTimer_Err:
'
'        Err.Raise vbObjectError + 100, "Reseter.cTimer.IsHiTimer", "cTimer component failure"
'        '</EhFooter>
'End Property
