VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "SHDOCVW.dll"
Begin VB.Form Principal 
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   9870
   ClientLeft      =   45
   ClientTop       =   360
   ClientWidth     =   12330
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9870
   ScaleWidth      =   12330
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdNavegar 
      Caption         =   "Ir"
      Default         =   -1  'True
      Height          =   495
      Left            =   11355
      TabIndex        =   4
      Top             =   60
      Width           =   900
   End
   Begin SHDocVwCtl.WebBrowser ie 
      Height          =   4320
      Left            =   30
      TabIndex        =   3
      Top             =   585
      Width           =   12225
      ExtentX         =   21564
      ExtentY         =   7620
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   1
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
   Begin VB.TextBox txtSalido 
      Height          =   1365
      Left            =   30
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   8220
      Width           =   12225
   End
   Begin VB.TextBox txtResultado 
      Height          =   3120
      Left            =   30
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   5040
      Width           =   12225
   End
   Begin VB.TextBox txtAnalizar 
      Height          =   450
      Left            =   30
      TabIndex        =   0
      Top             =   60
      Width           =   11265
   End
End
Attribute VB_Name = "Principal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Print2(Texto As String)
        '<EhHeader>
        On Error GoTo Print2_Err
        '</EhHeader>
        Static Buffer As String

        txtResultado.Text = txtResultado.Text & Texto & vbNewLine
        txtResultado.SelStart = Len(txtResultado.Text)
        '<EhFooter>
        Exit Sub

Print2_Err:

        controlar_error Erl, Err.Description, "Analizador_Web.Principal.Print2"
        Resume Next
        '</EhFooter>
End Sub

Public Sub controlar_error(algo, algo2, algo3)
'
End Sub

Private Sub cmdNavegar_Click()
ie.Navigate txtAnalizar.Text
End Sub

Private Sub Form_Load()
Caption = "Analizador Web V." & App.Major & "." & App.Minor & " para SVCommunty.org & TodoSV.com"
ie.Navigate2 "sms.todosv.com"
End Sub

Private Sub Analizar()
        '<EhHeader>
        On Error GoTo Analizar_Err
        '</EhHeader>
        Dim i As Integer
        Dim a As Integer
        Dim e As Integer
        Dim iFra As Byte
        Dim iMaxForm As Integer
        Dim iMaxFra As Integer
        Dim iMaxCon As Integer
        Dim Buffer As String
        On Error Resume Next
        txtAnalizar.Text = ie.LocationURL

        'txtResultado.Text = vbNullString
100     With ie

101         Do While .Busy
102             DoEvents
            Loop

            If ie.Document Is Nothing Then
                Print2 "Abortando analisis"
                Exit Sub
            End If

            'Numero de frames
105         iMaxFra = .Document.frames().length - 1
106         Print2 "P�gina: " & ie.LocationURL
107         Print2 "Titulo P�gina: " & ie.LocationName
108         Print2 "N�mero de cuadros: " & iMaxFra + 1

109         If iMaxFra <> -1 Then

111             For i = 0 To iMaxFra
112                 Print2 "-Nombre de cuadro " & i & ": " & .Document.frames(i).Name & "(" & ie.Document.getElementsByTagName("frame").Item(i).GetAttribute("src") & ")"
113                 iMaxForm = .Document.frames(i).Document.Forms.length - 1
114                 Print2 "--No. formularios en cuadro " & i & ": " & iMaxForm + 1

115                 For a = 0 To iMaxForm
116                     iMaxCon = .Document.frames(i).Document.Forms(a).length - 1
126                     Print2 "--Controles en formulario " & a + 1 & ":"

118                     For e = 0 To iMaxCon
119                         Print2 "----Control " & i & "," & a & "," & e & " : " & .Document.frames(i).Document.Forms(a)(e).Value & " :: " & .Document.frames(i).Document.Forms(a)(e).Name
                        Next
                    Next
                Next

            Else
121             iMaxForm = .Document.Forms.length - 1
122             Print2 "-Numero de formularios: " & iMaxForm + 1

123             If iMaxForm <> -1 Then

124                 For a = 0 To iMaxForm
125                     iMaxCon = .Document.Forms(a).length - 1
1                     Print2 "--Controles en formulario " & a + 1 & ":"

127                     For e = 0 To iMaxCon
128                         Print2 "---Control " & a & "," & e & " : " & .Document.Forms(a)(e).Value & " :: " & .Document.Forms(a)(e).Name
                        Next
                    Next

                Else
129                 Print2 "!!-> La pagina no ten�a formularios"
                End If
            End If

131         txtSalido.Text = .Document.documentelement.outerHTML
        End With

        '<EhFooter>
        Exit Sub
Analizar_Err:
        controlar_error Erl, Err.Description, "Analizador_Web.Principal.Analizar.Ref 12/2/2008 : 10:18:10"
        Resume Next
        '</EhFooter>
End Sub

Private Sub ie_BeforeNavigate2(ByVal pDisp As Object, _
                               URL As Variant, _
                               Flags As Variant, _
                               TargetFrameName As Variant, _
                               PostData As Variant, _
                               Headers As Variant, _
                               Cancel As Boolean)
    Dim sHeaders As String, sPostdata As String
    sHeaders = Replace$(StrConv(Headers, vbUnicode), Chr(0), vbNullString)
    sPostdata = Replace$(StrConv(PostData, vbUnicode), Chr(0), vbNullString)

    If Len(sHeaders) Or Len(sPostdata) Then
        Print2 "->" & Time$
        If Len(sHeaders) Then Print2 "HEADERS: " & sHeaders
        If Len(sPostdata) Then Print2 "POSTDATA: " & sPostdata
    End If

End Sub

Private Sub ie_DocumentComplete(ByVal pDisp As Object, URL As Variant)
Print2 "<-" & Time$
Analizar
End Sub
