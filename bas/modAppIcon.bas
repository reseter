Attribute VB_Name = "modAppIcon"
' *************************************************************************
'  Copyright �1999 Karl E. Peterson
'  All Rights Reserved, http://www.mvps.org/vb
' *************************************************************************
'  You are free to use this code within your own applications, but you
'  are expressly forbidden from selling or otherwise distributing this
'  source code, non-compiled, without prior written consent.
' *************************************************************************
Option Explicit
Private Declare Function GetWindowLong _
                Lib "user32" _
                Alias "GetWindowLongA" (ByVal hWnd As Long, _
                                        ByVal nIndex As Long) As Long
Private Declare Function SendMessage _
                Lib "user32" _
                Alias "SendMessageA" (ByVal hWnd As Long, _
                                      ByVal wMsg As Long, _
                                      ByVal wParam As Long, _
                                      lParam As Any) As Long
Private Declare Function LoadIcon _
                Lib "user32" _
                Alias "LoadIconA" (ByVal hInstance As Long, _
                                   lpIconName As Any) As Long
Private Const GWL_HWNDPARENT = (-8)
Private Const WM_GETICON = &H7F
Private Const WM_SETICON = &H80
Private Const ICON_SMALL = 0
Private Const ICON_BIG = 1
Private Const IDI_WINLOGO = 32517&

Public Function SetAppIcon(obj As Object) As Boolean
    '<EhHeader>
    On Error GoTo SetAppIcon_Err
    '</EhHeader>
    Dim hIcon As Long
    Dim hWnd As Long
    Dim nRet As Long

    If TypeOf obj Is Form Or TypeOf obj Is MDIForm Then
        ' Get top-level hidden window
        nRet = GetWindowLong(obj.hWnd, GWL_HWNDPARENT)

        Do While nRet
            hWnd = nRet
            nRet = GetWindowLong(hWnd, GWL_HWNDPARENT)
        Loop

        ' Get a handle to icon
        hIcon = SendMessage(obj.hWnd, WM_GETICON, ICON_BIG, ByVal 0&)

        If hIcon = 0 Then
            ' Load default waving-flag logo
            hIcon = LoadIcon(0, ByVal IDI_WINLOGO)
        End If

        ' Pass form icon as new app icon
        Call SendMessage(hWnd, WM_SETICON, ICON_BIG, ByVal hIcon)
        ' See if change took
        SetAppIcon = (hIcon = SendMessage(hWnd, WM_GETICON, ICON_BIG, ByVal 0&))
    End If

    '<EhFooter>
    Exit Function
SetAppIcon_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modAppIcon.SetAppIcon"
    Resume Next
    '</EhFooter>
End Function
