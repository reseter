Attribute VB_Name = "NET"
Option Explicit
Dim hpObjetoIE As Long
Type IP
    Cambio As Boolean
    IP_Actual As String
End Type
Public Declare Function InternetGetConnectedState _
               Lib "wininet.dll" (ByRef lpdwFlags As Long, _
                                  ByVal dwReserved As Long) As Long
'Local system uses a LAN to connect to the Internet.
Public Const INTERNET_CONNECTION_LAN As Long = &H2
'Offline
Public Const INTERNET_CONNECTION_OFFLINE As Long = &H20

Public Function GetNetConnectString() As Boolean
    '<EhHeader>
    On Error GoTo GetNetConnectString_Err
    '</EhHeader>
    Dim dwflags As Long
    Dim msg As String

    If InternetGetConnectedState(dwflags, 0&) Then
        If dwflags And INTERNET_CONNECTION_LAN Then
            GetNetConnectString = True
        End If

        If dwflags And INTERNET_CONNECTION_OFFLINE Then
            GetNetConnectString = False
        End If

    Else
        GetNetConnectString = False
    End If

    '<EhFooter>
    Exit Function
GetNetConnectString_Err:
    Controlar_Error Erl, Err.Description, "Reseter.NET.GetNetConnectString"
    Resume Next
    '</EhFooter>
End Function

Public Function Cambio_IP() As IP
    'Idea original de sortux, implementado por Vlad y hosting por No-IP
    'Cambio: 22/03/07
    '1.0.16 -> Eliminado un "registrar "Enviando datos" innecesario
    'Cambio: 03/04/07
    '1.0.19 -> Detectar cambios
    '          Ahora es funci�n y devuelve del cambio de ip
    '          Forzar recarga de pagina
    '<EhHeader>
    On Error GoTo Cambio_IP_Err
    '</EhHeader>
    Static AntiguaIp As String
    Dim ActualIp As String
    ActualIp = Trim$(pSocket.HTML_GET)

    If Len(ActualIp) > 16 Or Len(ActualIp) < 8 Then
        Registrar "+IP P�blica: imposible obtener."
        Cambio_IP.Cambio = True
        Exit Function
    End If

    Cambio_IP.IP_Actual = ActualIp
    Cambio_IP.Cambio = (ActualIp = AntiguaIp)

    If AntiguaIp = vbNullString Then AntiguaIp = GetSetting("Reseter4.0", "Datos", "UltimaIP", vbNullString)
    Registrar "+IP P�blica: " & ActualIp & IIf(ActualIp = AntiguaIp, " (La IP no cambi�)", IIf(AntiguaIp = vbNullString, vbNullString, " (OK, antes era: " & AntiguaIp & ")"))
    AntiguaIp = ActualIp
    SaveSetting "Reseter4.0", "Datos", "UltimaIP", ActualIp
    '<EhFooter>
    Exit Function
Cambio_IP_Err:
    Controlar_Error Erl, Err.Description, "Reseter.NET.Cambio_IP"
    Resume Next
    '</EhFooter>
End Function

Public Sub Crear_Objeto_IE()
    '100     Registrar "+Objeto IE: creando..."
    '<EhHeader>
    On Error GoTo Crear_Objeto_IE_Err
    '</EhHeader>
    hpObjetoIE = SetTimer(0, 0, 0, AddressOf lpObjetoIE)
    '<EhFooter>
    Exit Sub
Crear_Objeto_IE_Err:
    Controlar_Error Erl, Err.Description, "Reseter.NET.Crear_Objeto_IE"
    Resume Next
    '</EhFooter>
End Sub

Public Sub Destruir_Conexion()
    '<EhHeader>
    On Error GoTo Destruir_Conexion_Err
    '</EhHeader>
    Set IE = Nothing
    '<EhFooter>
    Exit Sub
Destruir_Conexion_Err:
    Controlar_Error Erl, Err.Description, "Reseter.NET.Destruir_Conexion"
    Resume Next
    '</EhFooter>
End Sub

Public Function RenovarLAN() As Long
    '<EhHeader>
    On Error GoTo RenovarLAN_Err
    '</EhHeader>
    RenovarLAN = ShellExecute(frmPrincipal.hWnd, "", "ipconfig /renew all", "", "", 0)
    Registrar "~Renovaci�n concluy� en " & RenovarLAN
    '<EhFooter>
    Exit Function
RenovarLAN_Err:
    Controlar_Error Erl, Err.Description, "Reseter.NET.RenovarLAN"
    Resume Next
    '</EhFooter>
End Function

Public Sub res_Web()
    '<EhHeader>
    On Error GoTo res_Web_Err
    '</EhHeader>
    On Error GoTo subError
    'Reseteo via pagina web
    Registrar "+-[MODO WEB] Enviando datos"

    ' Si es reseteo WEB lo primero que tenemos que hacer es armar la direccion a la que
    ' vamos a navegar en base a los datos del preprocesador
    With IE

        'Procesamos la direcci�n a navegar en base al tipo de acci�n.
        Select Case m_Datos.accionTipo

            Case Is = ed_java
                'Si solo vamos a ejecutar Java, solo tenemos que pasar el comando como la direcci�n
                m_Datos.Direccion = m_Datos.accionEX

            Case Else
                'En el caso de que vayamos a navegar o hacer clic, tenemos que contruir la direci�n
                m_Datos.Direccion = "http://" & IIf(Len(m_Datos.usuario) <> 0, m_Datos.usuario & ":", vbNullString) & IIf(Len(m_Datos.clave) <> 0, m_Datos.clave & "@", vbNullString) & m_Datos.base & ":" & IIf(IsNumeric(m_Datos.puerto), m_Datos.puerto, 80) & m_Datos.accionEX
        End Select

        If flag_Navegar Then .Navigate m_Datos.Direccion
        Registrar "Res_Web => armado: '" & m_Datos.Direccion & "'"

        Do While .Busy
            Esperar 0.1
        Loop

        Do While .ReadyState <> 4
            Esperar 0.1
        Loop

        frmWeb.txtLog.Text = "!!! " & m_Datos.Direccion

        Select Case m_Datos.accionTipo

            Case ed_clic

                If m_Datos.nForm = -1 And m_Datos.nCont = -1 Then
                    Registrar "# de Formulario y Control invalido."
                Else

                    If (.Document.Forms.Length - 1) >= m_Datos.nForm Then
                        If .Document.Forms(m_Datos.nForm).Length - 1 >= m_Datos.nCont Then
                            .Document.Forms(m_Datos.nForm)(m_Datos.nCont).Click
                            Registrar "Res_Web: Datos enviados [" & m_Datos.nForm & ", " & m_Datos.nCont & "]"
                        Else
                            Registrar "Res_Web: Err -> No existian suficientes controles"
                            NetError = True
                        End If

                    Else
                        Registrar "Res_Web: Err -> No existian suficientes formularios"
                        NetError = True
                    End If
                End If

            Case ed_llenar

                If m_Datos.nForm = -1 And m_Datos.nCont = -1 Then
                    Registrar "Sin datos, saltando de Llenado"
                Else

                    If (.Document.Forms.Length - 1) >= m_Datos.nForm Then
                        If .Document.Forms(m_Datos.nForm).Length - 1 >= m_Datos.nCont Then
                            .Document.Forms(m_Datos.nForm)(m_Datos.nCont).Value = m_Datos.accionEX2
                            Registrar "Res_Web: Texto llenado"
                        Else
                            Registrar "Res_Web: Err -> No existian suficientes controles"
                            NetError = True
                        End If

                    Else
                        Registrar "Res_Web: Err -> No existian suficientes formularios"
                        NetError = True
                    End If
                End If

            Case ed_java
                Registrar "Res_Web: JAVA excutado"

            Case ed_navegar
                'No hacer nada
        End Select

    End With

    Exit Sub
subError:
    NetError = True

    Select Case Err.Number

        Case 91
            Registrar "++Error -> probablemente el router es incorrecto"

        Case Else
            Registrar "++Error -> se desconoce la causa para un error #" & Err.Number
    End Select

    '<EhFooter>
    Exit Sub
res_Web_Err:
    Controlar_Error Erl, Err.Description, "Reseter.NET.res_Web"
    Resume Next
    '</EhFooter>
End Sub

Public Sub res_Telnet()
    'Reseteo via telnet
    '28/04/07 - 2.0.4: Unido con "Iniciar_Telnet", "Iniciar_Telnet" eliminado
    '<EhHeader>
    On Error GoTo res_Telnet_Err
    '</EhHeader>
    Registrar "+-[MODO TELNET] Enviando datos"
    '102     MsgBox "Telnet Iniciado"
    TelnetComandos() = Split(m_Datos.accionEX, ";")
    Registrar "No. de comandos a enviar: " & UBound(TelnetComandos) + 1
    nComando = 0
    Call frmTelnet.ProcTelnet
    '<EhFooter>
    Exit Sub
res_Telnet_Err:
    Controlar_Error Erl, Err.Description, "Reseter.NET.res_Telnet"
    Resume Next
    '</EhFooter>
End Sub

'CSEH: Skip
Public Sub lpObjetoIE()
    On Error Resume Next
    KillTimer 0, hpObjetoIE
    Set IE = CreateObject("InternetExplorer.Application")

    If IE Is Nothing Then
        Registrar "+Objeto IE: no creado"
        Exit Sub
    End If

    IE.RegisterAsBrowser = True
    IE.Visible = False
    IE.Offline = False
    IE.Silent = False
    Registrar "+Objeto IE: creado"
End Sub

Public Sub res_auro()
    'Reseteo via pagina web
    '<EhHeader>
    On Error GoTo res_auro_Err
    '</EhHeader>
    Registrar "+-[MODO WEB/AURONET] Enviando datos"

    With ppSocket
        .Direccion = m_Datos.Direccion
    End With

    '<EhFooter>
    Exit Sub
res_auro_Err:
    Controlar_Error Erl, Err.Description, "Reseter.NET.res_auro"
    Resume Next
    '</EhFooter>
End Sub
