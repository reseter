Attribute VB_Name = "GUI"
Option Explicit
'   Este archivo es parte del programa "reseter", el c�al es pertenece a SVCommunity.org y a Todosv.com
'   Mantenedores principales:
'    *Vlad
Rem Detener la actualizaci�n de las ventanas
Public Declare Function LockWindowUpdate _
               Lib "user32" (ByVal hwndLock As Long) As Long
Private Type tagInitCommonControlsEx
    lngSize As Long
    lngICC As Long
End Type
Private Declare Function InitCommonControlsEx _
                Lib "comctl32.dll" (iccex As tagInitCommonControlsEx) As Boolean
Private Const ICC_USEREX_CLASSES = &H200

Public Sub Colorear(Destino As Object)
    '<EhHeader>
    On Error GoTo Colorear_Err
    '</EhHeader>
    Dim c As Control
    Dim cControles As Object
    LockWindowUpdate Destino.hdc
    Destino.BackColor = vbWhite
    Set cControles = Destino.Controls

    With c

        For Each c In cControles

            If c.Tag = -1 Then GoTo NoProcesar

            Select Case TypeName(c)

                Case "Label"

                    Select Case c.Tag

                        Case ""
                            c.ForeColor = &HC00000
                            c.BackColor = vbWhite
                            c.BorderStyle = 0
                            c.Font = "Verdana"
                            c.FontBold = True
                            c.FontSize = 12
                            c.BackStyle = 0

                        Case 1
                            c.ForeColor = &H777777
                            c.BackColor = vbWhite
                            c.BorderStyle = 0
                            c.Font = "Arial"
                            c.FontBold = False
                            c.FontSize = 10

                        Case 2
                            c.ForeColor = vbBlack
                            c.BackColor = vbWhite
                            c.BorderStyle = 1
                            c.Font = "Courier New"
                            c.FontBold = False
                            c.FontSize = 8

                        Case 3
                            c.ForeColor = vbBlack
                            c.BackColor = vbWhite
                            c.BorderStyle = 0
                            c.Font = "Courier New"
                            c.FontBold = False
                            c.FontSize = 8

                        Case 4
                            c.ForeColor = vbBlue
                            c.BackColor = vbWhite
                            c.BorderStyle = 0
                            c.Font = "MS Sans Serif"
                            c.FontBold = True
                            c.FontSize = 8

                        Case 5
                            c.ForeColor = vbBlack
                            c.BackColor = vbWhite
                            c.BorderStyle = 0
                            c.Font = "Arial"
                            c.FontBold = False
                            c.FontSize = 8
                    End Select

                Case "CheckBox"
                    c.Alignment = 1
                    c.Appearance = 1
                    c.BackColor = vbWhite

                Case "OptionButton"
                    c.Alignment = 1
                    c.Appearance = 1
                    c.BackColor = vbWhite

                Case "CommandButton"

                    Select Case c.Tag

                        Case ""
                            c.BackColor = &HAAAAAA
                            c.Font = "Courier New"
                            c.FontBold = True
                            c.Font.Size = 10

                        Case 1
                            c.Font = "Courier New"
                            c.FontBold = False
                            c.Font.Size = 8
                    End Select

                Case "TextBox"

                    Select Case c.Tag

                        Case ""
                            c.Font = "Courier New"
                            c.FontSize = 10
                            c.FontBold = False
                            c.ForeColor = &HC00000
                            c.BackColor = &HFFFAFA
                            c.BorderStyle = 1

                        Case 1
                            c.ForeColor = vbRed
                            c.BackColor = &HFFFAFA
                            c.BorderStyle = 1
                            c.Appearance = 1
                            c.Font = "Courier New"
                            c.FontBold = False
                            c.FontSize = 8
                    End Select

                Case "Frame"
                    c.Font = "Arial"
                    c.BorderStyle = 1
                    c.ForeColor = &HC00000
                    c.BackColor = vbWhite
                    c.FontSize = 12
                    c.FontBold = False

                Case "ListView"
                    c.Font = "Courier New"
                    c.Font.Size = 9
                    c.Font.Bold = False
                    c.ForeColor = &HC00000
                    c.BackColor = &HFFFAFA
                    c.BorderStyle = 1

                Case "PictureBox"
                    c.BackColor = vbWhite

                Case "ComboBox"
                    c.ForeColor = &H777777
                    c.BackColor = vbWhite
                    c.Font = "Arial"
                    c.FontBold = False
                    c.FontSize = 10
            End Select

NoProcesar:
        Next

    End With

    LockWindowUpdate 0
    '<EhFooter>
    Exit Sub
Colorear_Err:
    Controlar_Error Erl, Err.Description, "Reseter.GUI.Colorear"
    Resume Next
    '</EhFooter>
End Sub

Public Sub Mostrar_Datos()
    '<EhHeader>
    On Error GoTo Mostrar_Datos_Err

    '</EhHeader>
    With frmPrincipal
        .txtCodigos.Text = Replace(m_Datos.codigo, Chr(254), vbNewLine)
    End With

    '<EhFooter>
    Exit Sub
Mostrar_Datos_Err:
    Controlar_Error Erl, Err.Description, "Reseter.GUI.Mostrar_Datos"
    Resume Next
    '</EhFooter>
End Sub

Public Sub lpActualizar()
    '<EhHeader>
    On Error GoTo lpActualizar_Err
    '</EhHeader>
    Dim Buff As Double
    Buff = Tiempo.Elapsed
    frmPrincipal.lblTW.Caption = Fix((Umbral_EsperarInternetMax - Buff) / 1000)

    If Buff > Umbral_EsperarInternetMax Or NetError Then pSocket.error = True
    '<EhFooter>
    Exit Sub
lpActualizar_Err:
    Controlar_Error Erl, Err.Description, "Reseter.GUI.lpActualizar"
    Resume Next
    '</EhFooter>
End Sub

Public Sub Registrar(ByVal Texto As String, _
                     Optional compat As Integer = 0)
    '<EhHeader>
    On Error GoTo Registrar_Err

    '</EhHeader>
    If compat > 2 Then Exit Sub
    frmPrincipal.txtSalida.Text = frmPrincipal.txtSalida & Time$ & " " & Texto & vbNewLine
    frmPrincipal.txtSalida.SelStart = Len(frmPrincipal.txtSalida.Text)
    '<EhFooter>
    Exit Sub
Registrar_Err:
    Controlar_Error Erl, Err.Description, "Reseter.GUI.Registrar"
    Resume Next
    '</EhFooter>
End Sub
