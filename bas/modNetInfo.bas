Attribute VB_Name = "modNetInfo"
Public Const MAX_HOSTNAME_LEN = 132
Public Const MAX_DOMAIN_NAME_LEN = 132
Public Const MAX_SCOPE_ID_LEN = 260
Public Const MAX_ADAPTER_NAME_LENGTH = 260
Public Const MAX_ADAPTER_ADDRESS_LENGTH = 8
Public Const MAX_ADAPTER_DESCRIPTION_LENGTH = 132
Public Const ERROR_BUFFER_OVERFLOW = 111
Public Const MIB_IF_TYPE_ETHERNET = 1
Public Const MIB_IF_TYPE_TOKENRING = 2
Public Const MIB_IF_TYPE_FDDI = 3
Public Const MIB_IF_TYPE_PPP = 4
Public Const MIB_IF_TYPE_LOOPBACK = 5
Public Const MIB_IF_TYPE_SLIP = 6
Type IP_ADDR_STRING
    iNext As Long
    IpAddress As String * 16
    IpMask As String * 16
    Context As Long
End Type
Type IP_ADAPTER_INFO
    iNext As Long
    ComboIndex As Long
    AdapterName As String * MAX_ADAPTER_NAME_LENGTH
    Description As String * MAX_ADAPTER_DESCRIPTION_LENGTH
    AddressLength As Long
    Address(MAX_ADAPTER_ADDRESS_LENGTH - 1) As Byte
    Index As Long
    Type As Long
        DhcpEnabled As Long
        CurrentIpAddress As Long
        IpAddressList As IP_ADDR_STRING
        GatewayList As IP_ADDR_STRING
        DhcpServer As IP_ADDR_STRING
        HaveWins As Boolean
        PrimaryWinsServer As IP_ADDR_STRING
        SecondaryWinsServer As IP_ADDR_STRING
        LeaseObtained As Long
        LeaseExpires As Long
    End Type
    Type FIXED_INFO
        HostName As String * MAX_HOSTNAME_LEN

        DomainName As String * MAX_DOMAIN_NAME_LEN
        CurrentDnsServer As Long
        DnsServerList As IP_ADDR_STRING
        NodeType As Long
        ScopeId  As String * MAX_SCOPE_ID_LEN
        EnableRouting As Long
        EnableProxy As Long
        EnableDns As Long
    End Type
    Public Declare Function GetNetworkParams _
                   Lib "IPHlpApi" (FixedInfo As Any, _
                                   pOutBufLen As Long) As Long
    Public Declare Function GetAdaptersInfo _
                   Lib "IPHlpApi" (IpAdapterInfo As Any, _
                                   pOutBufLen As Long) As Long
    Public Declare Sub CopyMemory _
                   Lib "kernel32" _
                   Alias "RtlMoveMemory" (Destination As Any, _
                                          Source As Any, _
                                          ByVal Length As Long)

Sub ObtenerNetInfo()
    'This example was created by George Bernier (bernig@dinomail.qc.ca)
    '<EhHeader>
    On Error GoTo ObtenerNetInfo_Err
    '</EhHeader>
    Dim error As Long
    Dim FixedInfoSize As Long
    Dim AdapterInfoSize As Long
    Dim i As Integer
    Dim PhysicalAddress  As String
    Dim NewTime As Date
    Dim AdapterInfo As IP_ADAPTER_INFO
    Dim Adapt As IP_ADAPTER_INFO
    Dim AddrStr As IP_ADDR_STRING
    Dim FixedInfo As FIXED_INFO
    Dim Buffer As IP_ADDR_STRING
    Dim pAddrStr As Long
    Dim pAdapt As Long
    Dim Buffer2 As IP_ADAPTER_INFO
    Dim FixedInfoBuffer() As Byte
    Dim AdapterInfoBuffer() As Byte
    'Get the main IP configuration information for this machine using a FIXED_INFO structure
    FixedInfoSize = 0
    error = GetNetworkParams(ByVal 0&, FixedInfoSize)

    If error <> 0 Then
        If error <> ERROR_BUFFER_OVERFLOW Then
            MsgBox "GetNetworkParams sizing failed with error " & error
            Exit Sub
        End If
    End If

    ReDim FixedInfoBuffer(FixedInfoSize - 1)
    error = GetNetworkParams(FixedInfoBuffer(0), FixedInfoSize)

    If error = 0 Then
        CopyMemory FixedInfo, FixedInfoBuffer(0), Len(FixedInfo)
        frm1.lblhost = FixedInfo.HostName  'host name
        frm1.lbldns1 = FixedInfo.DnsServerList.IpAddress 'dns server IP
        pAddrStr = FixedInfo.DnsServerList.iNext

        Do While pAddrStr <> 0
            CopyMemory Buffer, ByVal pAddrStr, Len(Buffer)
            frm1.lbldns2 = Buffer.IpAddress 'dns server IP
            pAddrStr = Buffer.iNext
        Loop

        Select Case FixedInfo.NodeType 'node type

            Case 1
                frm1.lblnode = "Broadcast"

            Case 2
                frm1.lblnode = "Peer to peer"

            Case 4
                frm1.lblnode = "Mixed"

            Case 8
                frm1.lblnode = "Hybrid"

            Case Else
                frm1.lblnode = "Unknown"
        End Select

        If FixedInfo.ScopeId = "" Then
            frm1.lblscope = "Not Found"
        Else
            frm1.lblscope = FixedInfo.ScopeId 'scope ID
        End If

        'routing
        If FixedInfo.EnableRouting Then
            frm1.lblroute = "Enabled"
        Else
            frm1.lblroute = "Disabled"
        End If

        ' proxy
        If FixedInfo.EnableProxy Then
            frm1.lblproxy = "Enabled"
        Else
            frm1.lblproxy = "Disabled"
        End If

        ' netbios
        If FixedInfo.EnableDns Then
            frm1.lblres = "Using DNS"
        Else
            frm1.lblres = "Not using DNS"
        End If

    Else
        frm1.lblres = error
        Exit Sub
    End If

    'Enumerate all of the adapter specific information using the IP_ADAPTER_INFO structure.
    'Note:  IP_ADAPTER_INFO contains a linked list of adapter entries.
    AdapterInfoSize = 0
    error = GetAdaptersInfo(ByVal 0&, AdapterInfoSize)

    If error <> 0 Then
        If error <> ERROR_BUFFER_OVERFLOW Then
            frm1.lblinfosize = error
            Exit Sub
        End If
    End If

    ReDim AdapterInfoBuffer(AdapterInfoSize - 1)
    ' Get actual adapter information
    error = GetAdaptersInfo(AdapterInfoBuffer(0), AdapterInfoSize)

    If error <> 0 Then
        MsgBox "GetAdaptersInfo failed with error " & error
        Exit Sub
    End If

    CopyMemory AdapterInfo, AdapterInfoBuffer(0), Len(AdapterInfo)
    pAdapt = AdapterInfo.iNext

    Do While pAdapt <> 0
        CopyMemory Buffer2, AdapterInfo, Len(Buffer2)

        Select Case Buffer2.Type

            Case MIB_IF_TYPE_ETHERNET
                frm1.lblatype = "Ethernet adapter "

            Case MIB_IF_TYPE_TOKENRING
                frm1.lblatype = "Token Ring adapter "

            Case MIB_IF_TYPE_FDDI
                frm1.lblatype = "FDDI adapter "

            Case MIB_IF_TYPE_PPP
                frm1.lblatype = "PPP adapter"

            Case MIB_IF_TYPE_LOOPBACK
                frm1.lblatype = "Loopback adapter "

            Case MIB_IF_TYPE_SLIP
                frm1.lblatype = "Slip adapter "

            Case Else
                frm1.lblatype = "Other adapter "
        End Select

        frm1.lbladname = Buffer2.AdapterName
        frm1.lbldesc = Buffer2.Description  'adatpter name

        For i = 0 To Buffer2.AddressLength - 1
            PhysicalAddress = PhysicalAddress & Hex(Buffer2.Address(i))

            If i < Buffer2.AddressLength - 1 Then
                PhysicalAddress = PhysicalAddress & "-"
            End If

        Next

        frm1.lblphy = PhysicalAddress 'mac address

        If Buffer2.DhcpEnabled Then
            frm1.lbldhcpstatus = "Enabled"
        Else
            frm1.lbldhcpstatus = "Disabled"
        End If

        pAddrStr = Buffer2.IpAddressList.iNext

        Do While pAddrStr <> 0
            frm1.lblip = Buffer.IpAddress
            frm1.lblsub = Buffer.IpMask
            pAddrStr = Buffer.iNext

            If pAddrStr <> 0 Then
                CopyMemory Buffer2.IpAddressList, ByVal pAddrStr, Len(Buffer2.IpAddressList)
            End If

        Loop

        frm1.lblgate = Buffer2.GatewayList.IpAddress 'Gateway
        pAddrStr = Buffer2.GatewayList.iNext

        Do While pAddrStr <> 0
            CopyMemory Buffer, Buffer2.GatewayList, Len(Buffer)
            frm1.lblip = "IP Address: " & Buffer.IpAddress
            pAddrStr = Buffer.iNext

            If pAddrStr <> 0 Then
                CopyMemory Buffer2.GatewayList, ByVal pAddrStr, Len(Buffer2.GatewayList)
            End If

        Loop

        frm1.lbldhcpserver = Buffer2.DhcpServer.IpAddress
        frm1.lblpriwins = Buffer2.PrimaryWinsServer.IpAddress
        frm1.lblsecwins = Buffer2.SecondaryWinsServer.IpAddress
        ' Display time
        NewTime = CDate(Adapt.LeaseObtained)
        frm1.lbldhcplease = CStr(NewTime)
        NewTime = CDate(Adapt.LeaseExpires)
        frm1.lbldhcpexpiration = CStr(NewTime)
        pAdapt = Buffer2.iNext

        If pAdapt <> 0 Then
            CopyMemory AdapterInfo, ByVal pAdapt, Len(AdapterInfo)
        End If

    Loop

    '<EhFooter>
    Exit Sub
ObtenerNetInfo_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modNetInfo.ObtenerNetInfo"
    Resume Next
    '</EhFooter>
End Sub
