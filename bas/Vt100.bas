Attribute VB_Name = "vt100"
Option Explicit
'Windows RECT structure
Private Type RECT
    Left    As Long
    Top     As Long
    Right   As Long
    bottom  As Long
End Type
Private Declare Function ScrollDC _
                Lib "user32" (ByVal hdc As Long, _
                              ByVal dx As Long, _
                              ByVal dy As Long, _
                              lprcScroll As RECT, _
                              lprcClip As RECT, _
                              ByVal hRgnUpdate As Long, _
                              lprcUpdate As RECT) As Long
Private Declare Function PatBlt _
                Lib "gdi32" (ByVal hdc As Long, _
                             ByVal x As Long, _
                             ByVal y As Long, _
                             ByVal nWidth As Long, _
                             ByVal nHeight As Long, _
                             ByVal dwRop As Long) As Long
Private Declare Function SetBkMode _
                Lib "gdi32" (ByVal hdc As Long, _
                             ByVal nBkMode As Long) As Long
Private Declare Function TextOut _
                Lib "gdi32" _
                Alias "TextOutA" (ByVal hdc As Long, _
                                  ByVal x As Long, _
                                  ByVal y As Long, _
                                  ByVal lpString As String, _
                                  ByVal nCount As Long) As Long
Private Declare Function SetBkColor _
                Lib "gdi32" (ByVal hdc As Long, _
                             ByVal crColor As Long) As Long
Private Declare Function GetBkColor _
                Lib "gdi32" (ByVal hdc As Long) As Long
Private Declare Function GetTextColor _
                Lib "gdi32" (ByVal hdc As Long) As Long
Private Declare Function SetTextColor _
                Lib "gdi32" (ByVal hdc As Long, _
                             ByVal newcolor As Long) As Long
Private Declare Function IsCharAlpha _
                Lib "user32" _
                Alias "IsCharAlphaA" (ByVal cChar As Byte) As Long
'=================== Ternary raster operations ============
'Private Const PATCOPY = &HF00021         ' (DWORD) dest = pattern
'Private Const PATPAINT = &HFB0A09        ' (DWORD) dest = DPSnoo
'Private Const PATINVERT = &H5A0049       ' (DWORD) dest = pattern XOR dest
Private Const DSTINVERT = &H550009       ' (DWORD) dest = (NOT dest)
'Private Const BLACKNESS = &H42&          ' (DWORD) dest = BLACK
'Private Const WHITENESS = &HFF0062       ' (DWORD) dest = WHITE
'Private Const TRANSPARENT = 1
Private Const OPAQUE = 2
Private Const GO_IAC1 = 6
Private Const LinesPerPage = 25
Private Const CharsPerLine = 80
Private Const TabsPerPage = 20
Private Const LastLine = LinesPerPage - 1
Private Const LastChar = CharsPerLine - 1
'Private Const LastTab = 19
Private ScrImage(LinesPerPage)    As String * CharsPerLine
Private ScrAttr(LinesPerPage)     As String * CharsPerLine
Private Norm_Attr                 As String * CharsPerLine
Private Blank_Line                As String * CharsPerLine
Private TermTextColor             As Long
Private TermBkColor               As Long
Private tabno                     As Integer
Private tab_table(TabsPerPage)    As Integer
Private curattr                   As String
Private lprcScroll                As RECT
Private lprcClip                  As RECT
Private hRgnUpdate                As Integer
Private lprcUpdate                As RECT
'
'   Current Buffered Text waiting for output on screen
'
'Private outlen          As Integer
'
'   Flag to indicate that we're ready to run
'
Private FlagInit        As Integer
Private CurX            As Integer
Private CurY            As Integer
Private SavecurX        As Integer
Private SavecurY        As Integer
Private InEscape        As Boolean    ' Processing an escape seq?
Private EscString       As String     ' String so far
Private charheight      As Single
Private charWidth       As Single
Private CurState        As Boolean

Public Function term_process_char(CH As Byte)
    '<EhHeader>
    On Error GoTo term_process_char_Err

    '</EhHeader>
    If (InEscape) Then
        Call term_escapeProcess(CH)
    Else

        Select Case CH

            Case 0

            Case 7
                Beep

            Case 8

                If CurX > 0 Then                    '   if not at line begin
                    CurX = CurX - 1                 '   adjust back 1 spc
                End If

            Case 9
                Dim tY As Integer

                For tY = 0 To 19

                    If CurY < tab_table(tY) Then
                        Exit For
                    End If

                Next

                CurY = tab_table(tY)

            Case 10, 11, 12

                If (CurY = LastLine) Then           '   if line left on scrn
                    Call term_scroll_up             '   ..  scroll upwards
                    CurY = LastLine                 '   ..  use blank line
                Else
                    CurY = CurY + 1                 '   goto next line
                End If

            Case 13
                CurX = 0

            Case 27
                InEscape = True
                EscString = ""

            Case 255
                term_process_char = GO_IAC1

            Case Else
                ' if (CH > 31) Then ' And (CH < 128)
                term_write CH
                Mid$(ScrImage(CurY + 1), CurX, 1) = Chr$(CH)
                Mid$(ScrAttr(CurY + 1), CurX, 1) = curattr
                ' End If
        End Select

    End If

    '<EhFooter>
    Exit Function
term_process_char_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_process_char"
    Resume Next
    '</EhFooter>
End Function

Public Sub term_CaretControl(TurnOff As Boolean)
    '<EhHeader>
    On Error GoTo term_CaretControl_Err
    '</EhHeader>
    Static SaveState As Boolean

    If TurnOff = True Then
        SaveState = CurState
        term_Carethide
    Else

        If SaveState = True Then
            term_Caretshow
        End If
    End If

    '<EhFooter>
    Exit Sub
term_CaretControl_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_CaretControl"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_Carethide()
    '<EhHeader>
    On Error GoTo term_Carethide_Err

    '</EhHeader>
    If CurState = True Then
        If frmTelnet.WindowState <> 1 Then PatBlt frmTelnet.hdc, CurX * charWidth, CurY * charheight, charWidth, charheight, DSTINVERT
        CurState = False
    End If

    '<EhFooter>
    Exit Sub
term_Carethide_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_Carethide"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_Caretshow()
    '------------------------------------------------------------------------
    '   term_CaretShow
    '
    '   display the inverted block cursor on the screen.
    '   currently uses PatBlt.
    '------------------------------------------------------------------------
    '<EhHeader>
    On Error GoTo term_Caretshow_Err

    '</EhHeader>
    If frmTelnet.WindowState <> 1 Then PatBlt frmTelnet.hdc, CurX * charWidth, CurY * charheight, charWidth, charheight, DSTINVERT
    CurState = True
    '<EhFooter>
    Exit Sub
term_Caretshow_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_Caretshow"
    Resume Next
    '</EhFooter>
End Sub

Public Sub term_DriveCursor()
    '<EhHeader>
    On Error GoTo term_DriveCursor_Err

    '</EhHeader>
    If CurState = False Then
        Call term_Caretshow
    Else
        Call term_Carethide
    End If

    '<EhFooter>
    Exit Sub
term_DriveCursor_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_DriveCursor"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_eraseBOL()
    '------------------------------------------------------------------------
    '   term_eraseBOL
    '   erase from beginning of current line
    '------------------------------------------------------------------------
    '<EhHeader>
    On Error GoTo term_eraseBOL_Err

    '</EhHeader>
    If frmTelnet.WindowState <> 1 Then
        ' PatBlt(frmTelnet.hdc, 0, CurY * charheight, curX * charWidth, charheight, BLACKNESS)
        TextOut frmTelnet.hdc, 0, CurY * charheight, Blank_Line, CharsPerLine
    End If

    Mid$(ScrImage(CurY + 1), 1, CurX + 1) = Space$(CurX + 1)
    Mid$(ScrAttr(CurY + 1), 1, CurX + 1) = String$(CurX + 1, "0")
    '<EhFooter>
    Exit Sub
term_eraseBOL_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_eraseBOL"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_eraseBOS()
    '------------------------------------------------------------------------
    '   term_eraseBOS
    '   erase all lines from beginning of screen to and including current
    '------------------------------------------------------------------------
    '<EhHeader>
    On Error GoTo term_eraseBOS_Err
    '</EhHeader>
    Dim y As Integer
    'Erase the current line first
    Call term_eraseBOL

    'Erase everything up to current line
    If (CurY > 0) Then
        If frmTelnet.WindowState <> 1 Then TextOut frmTelnet.hdc, 0, 0, Space$(CharsPerLine * CurY + CurX), CharsPerLine * CurY + CurX

        ' reset screen buffer contents
        For y = 1 To CurY
            ScrImage(y) = Blank_Line
            ScrAttr(y) = Norm_Attr
        Next

    End If

    '<EhFooter>
    Exit Sub
term_eraseBOS_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_eraseBOS"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_eraseBUFFER()
    '<EhHeader>
    On Error GoTo term_eraseBUFFER_Err
    '</EhHeader>
    Dim i As Integer

    For i = 1 To LinesPerPage
        ScrImage(i) = Blank_Line
        ScrAttr(i) = Norm_Attr
    Next

    '<EhFooter>
    Exit Sub
term_eraseBUFFER_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_eraseBUFFER"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_eraseEOL()
    '
    '   Erase to End of Line
    '
    '<EhHeader>
    On Error GoTo term_eraseEOL_Err

    '</EhHeader>
    If frmTelnet.WindowState <> 1 Then TextOut frmTelnet.hdc, CurX * charWidth, CurY * charheight, Space$(CharsPerLine - CurX), CharsPerLine - CurX
    'Update screen buffer
    Mid$(ScrImage(CurY + 1), CurX + 1, CharsPerLine - CurX) = Space$(CharsPerLine - CurX)
    Mid$(ScrAttr(CurY + 1), CurX + 1, CharsPerLine - CurX) = String$(CharsPerLine - CurX, "0")
    '<EhFooter>
    Exit Sub
term_eraseEOL_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_eraseEOL"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_eraseEOS()
    '
    '   Erase to end of screen
    '
    '<EhHeader>
    On Error GoTo term_eraseEOS_Err
    '</EhHeader>
    Dim y As Integer
    Call term_eraseEOL

    If (CurY <> LastLine) Then
        If frmTelnet.WindowState <> 1 Then TextOut frmTelnet.hdc, 0, (CurY + 1) * charheight, Space$((LastLine - CurY) * CharsPerLine), (LastLine - CurY) * CharsPerLine

        For y = CurY + 2 To LinesPerPage
            ScrImage(y) = Blank_Line
            ScrAttr(y) = Norm_Attr
        Next

    End If

    '<EhFooter>
    Exit Sub
term_eraseEOS_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_eraseEOS"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_eraseLINE()
    '   Erase Line
    '<EhHeader>
    On Error GoTo term_eraseLINE_Err

    '</EhHeader>
    If frmTelnet.WindowState <> 1 Then TextOut frmTelnet.hdc, 0, CurY * charheight, Blank_Line, CharsPerLine
    ScrImage(CurY + 1) = Blank_Line
    ScrAttr(CurY + 1) = Norm_Attr
    '<EhFooter>
    Exit Sub
term_eraseLINE_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_eraseLINE"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_eraseSCREEN()
    'Assume that they want to repaint using the latest background color
    '<EhHeader>
    On Error GoTo term_eraseSCREEN_Err
    '</EhHeader>
    TermBkColor = GetBkColor(frmTelnet.hdc)
    TermTextColor = GetTextColor(frmTelnet.hdc)
    frmTelnet.BackColor = TermBkColor
    frmTelnet.ForeColor = TermTextColor
    term_eraseBUFFER
    frmTelnet.Cls
    CurX = 0
    CurY = 0
    '<EhFooter>
    Exit Sub
term_eraseSCREEN_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_eraseSCREEN"
    Resume Next
    '</EhFooter>
End Sub

Private Function term_escapeParseArg(s As String) As String
    '
    '   PopArg takes the next argument (digits up to a ;) and
    '   returns it.  It also removes the arg and the ; from
    '   the "s"
    '<EhHeader>
    On Error GoTo term_escapeParseArg_Err
    '</EhHeader>
    Dim i As Integer
    i = InStr(s, ";")

    If i = 0 Then
        term_escapeParseArg = s
        s = ""
    Else
        term_escapeParseArg = Left$(s, i - 1)
        s = Mid$(s, i + 1)
    End If

    '<EhFooter>
    Exit Function
term_escapeParseArg_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_escapeParseArg"
    Resume Next
    '</EhFooter>
End Function

Private Sub term_escapeProcess(CH As Byte)
    '<EhHeader>
    On Error GoTo term_escapeProcess_Err
    '</EhHeader>
    Dim c           As String
    Dim yDiff       As Integer
    Dim xDiff       As Integer
    c = Chr$(CH)

    If EscString = "" Then

        'No start character yet
        Select Case c

            Case "["

            Case "("

            Case ")"

            Case "#"

            Case Chr$(8)             ' embedded backspace
                CurX = CurX - 1
                term_validatecurX
                InEscape = False

            Case "7"                 ' save cursor
                'Save cursor position
                SavecurX = CurX
                SavecurY = CurY
                InEscape = False

            Case "8"                 ' restore cursor
                'restore cursor position
                CurX = SavecurX
                CurY = SavecurY
                InEscape = False

            Case "c"                 ' look at VSIreset()

            Case "D"                 ' cursor down
                CurY = CurY + 1
                term_validatecurY
                InEscape = False

            Case "E"                 ' next line
                CurY = CurY + 1
                CurX = 0
                term_validatecurY
                term_validatecurX
                InEscape = False

            Case "H"                 ' set tab
                tab_table(tabno) = CurY
                tabno = tabno + 1
                InEscape = False

            Case "I"                 ' look at bp_ESC_I()
                InEscape = False

            Case "M"                 ' cursor up
                CurY = CurY - 1
                term_validatecurY

            Case "Z"                 ' send ident
                InEscape = False

            Case Else
                InEscape = False
                Exit Sub
        End Select

    End If

    EscString = EscString & c

    If IsCharAlpha(CH) = 0 Then

        ' Not a character ...
        If Len(EscString) > 15 Then
            InEscape = False
        End If

        Exit Sub
    End If

    Select Case c

        Case "A"
            ' A ==> move cursor up
            EscString = Mid$(EscString, 2)
            yDiff = Val(term_escapeParseArg(EscString))

            If yDiff = 0 Then
                yDiff = 1
            End If

            CurY = CurY - yDiff
            term_validatecurY

        Case "B"
            ' B ==> move cursor down
            EscString = Mid$(EscString, 2)
            yDiff = Val(term_escapeParseArg(EscString))

            If yDiff = 0 Then
                yDiff = 1
            End If

            CurY = CurY + yDiff
            term_validatecurY

        Case "C"
            ' C ==> move cursor right
            EscString = Mid$(EscString, 2)
            xDiff = Val(term_escapeParseArg(EscString))

            If xDiff = 0 Then
                xDiff = 1
            End If

            CurX = CurX + xDiff
            term_validatecurX

        Case "D"
            ' D ==> move cursor left
            EscString = Mid$(EscString, 2)
            xDiff = Val(term_escapeParseArg(EscString))

            If xDiff = 0 Then
                xDiff = 1
            End If

            CurX = CurX - xDiff
            term_validatecurX

        Case "H"
            'Goto cursor position indicated by escape sequence
            EscString = Mid$(EscString, 2)
            CurY = Val(term_escapeParseArg(EscString)) - 1
            term_validatecurY
            CurX = Val(EscString) - 1
            term_validatecurX

        Case "J"

            'Erase display
            Select Case Val(Mid$(EscString, 2))

                Case 0

                    If CurX = 0 And CurY = 0 Then
                        Call term_eraseSCREEN
                    Else
                        Call term_eraseEOS
                    End If

                Case 1
                    Call term_eraseBOS

                Case 2
                    Call term_eraseSCREEN
            End Select

        Case "K"

            'Erase line
            Select Case Val(Mid$(EscString, 2))

                Case 0
                    'erase to end of line
                    Call term_eraseEOL

                Case 1
                    'erase to end of line
                    Call term_eraseBOL

                Case 2
                    Call term_eraseLINE
            End Select

        Case "f"
            'Goto cursor position indicated by escape sequence
            EscString = Mid$(EscString, 2)
            CurY = Val(term_escapeParseArg(EscString)) - 1
            term_validatecurY
            CurX = Val(EscString) - 1
            term_validatecurX

        Case "g"
            ' clear tabs
            Dim tY As Integer

            For tY = 0 To 19
                tab_table(tY) = 0
            Next

        Case "h"
            'restore cursor position
            CurX = SavecurX
            CurY = SavecurY

        Case "i"

            ' print though mode
        Case "l"
            'Save cursor position
            SavecurX = CurX
            SavecurY = CurY

        Case "m"
            'Change text attributes, screen colors
            EscString = Mid$(EscString, 2)

            Do
                Call term_setattr(Chr$(Val(term_escapeParseArg(EscString))))
            Loop While EscString <> ""

        Case "r"
            'Set scrollable region
            EscString = Mid$(EscString, 2)
            lprcScroll.Top = (Val(term_escapeParseArg(EscString)) - 1) * charheight
            lprcClip = lprcScroll

        Case "s"
            'Save cursor position
            SavecurX = CurX
            SavecurY = CurY

        Case "u"
            'restore cursor position
            CurX = SavecurX
            CurY = SavecurY
    End Select

    InEscape = False
    EscString = ""
    '<EhFooter>
    Exit Sub
term_escapeProcess_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_escapeProcess"
    Resume Next
    '</EhFooter>
End Sub

Public Sub term_init()
    '<EhHeader>
    On Error GoTo term_init_Err

    '</EhHeader>
    With frmTelnet
        .Cls
        'Get the pixel metrics of the current font
        .FontUnderline = False
        .FontItalic = False
        .FontBold = False
        .ScaleMode = 3
        charheight = frmTelnet.TextHeight("M")
        charWidth = frmTelnet.TextWidth("M")
        'Set up the vt100 screen
        .ScaleMode = 1
        .Height = (frmTelnet.Height - frmTelnet.ScaleHeight) + LinesPerPage * frmTelnet.TextHeight("M")
        .Width = (frmTelnet.Width - frmTelnet.ScaleWidth) + CharsPerLine * frmTelnet.TextWidth("M")
        'Set the user scale of the display
        .ScaleMode = 0
        .ScaleWidth = LinesPerPage
        .ScaleWidth = CharsPerLine
        .Scale 0 - LastChar, 0 - LastLine
        'Set up the scoll region and clip region structures
        lprcScroll.Top = 0
        lprcScroll.Left = 0
        lprcScroll.Right = CharsPerLine * charWidth
        lprcScroll.bottom = LinesPerPage * charheight
        lprcClip = lprcScroll
        hRgnUpdate = 0
        'Initialize module level flags and variables
        InEscape = False
        CurState = False
        curattr = "0"
        CurX = 0
        CurY = 0
        'Set the default foreground and background colors
        SetBkMode frmTelnet.hdc, OPAQUE
        .ForeColor = QBColor(15)
        .BackColor = QBColor(0)
        SetBkColor frmTelnet.hdc, frmTelnet.BackColor
        SetTextColor frmTelnet.hdc, frmTelnet.ForeColor
        TermTextColor = GetTextColor(frmTelnet.hdc)
        TermBkColor = GetBkColor(frmTelnet.hdc)
        'Initialize repaint buffer
        Norm_Attr = String$(CharsPerLine, "0")
        Blank_Line = Space$(CharsPerLine)
        term_eraseBUFFER
        FlagInit = True
        'Do the cursor thing
        term_Caretshow
        .cursor_timer.Enabled = True
    End With

    '<EhFooter>
    Exit Sub
term_init_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_init"
    Resume Next
    '</EhFooter>
End Sub

Private Function Term_FindChange(InArray As String, _
                                 ByVal CurrentValue As String, _
                                 ByteLen As Integer) As Integer
    '<EhHeader>
    On Error GoTo Term_FindChange_Err
    '</EhHeader>
    Dim RetValue As Integer
    Dim CurrentByte As Byte
    Dim InByte() As Byte
    CurrentByte = CurrentValue
    InByte = InArray

    For RetValue = 1 To ByteLen

        If InByte(RetValue) <> CurrentByte Then
            Exit For
        End If

    Next

    Term_FindChange = RetValue - 1
    '<EhFooter>
    Exit Function
Term_FindChange_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.Term_FindChange"
    Resume Next
    '</EhFooter>
End Function

Public Sub term_redrawscreen()
    '<EhHeader>
    On Error GoTo term_redrawscreen_Err

    '</EhHeader>
    If Not FlagInit Or frmTelnet.WindowState = 1 Then
        Exit Sub
    End If

    Dim oldcur      As Boolean
    Dim oldattr     As String
    Dim newattr     As String
    Dim y           As Integer
    Dim X1          As Integer
    Dim X2          As Integer
    Dim AttrChange  As Integer
    Dim tAttr       As String * CharsPerLine
    Dim tLine       As String * CharsPerLine
    oldcur = CurState
    oldattr = curattr

    If Not frmTelnet.Receiving Then
        Call term_Carethide
    End If

    Call term_setattr("0")

    For y = 1 To LinesPerPage
        tAttr = ScrAttr(y)
        tLine = ScrImage(y)

        If (tAttr = Norm_Attr) Then
            'Normal Lines can be repainted directly
            TextOut frmTelnet.hdc, 0, (y - 1) * charheight, tLine, CharsPerLine
        Else
            'Complex lines must have attribute changes found using the
            'Term_function FindChange.
            X1 = 1                          'Start the scan on the complete line
            X2 = CharsPerLine

            Do While (X2 > 0)
                AttrChange = Term_FindChange(Mid$(tAttr, X1, X2), curattr, X2)
                TextOut frmTelnet.hdc, (X1 - 1) * charWidth, (y - 1) * charheight, Mid$(tLine, X1, AttrChange), AttrChange
                X2 = X2 - AttrChange

                If X2 > 0 Then
                    X1 = X1 + AttrChange
                    newattr = Mid$(tAttr, X1, 1)

                    If newattr <> "0" Then
                        term_setattr newattr
                    Else
                        curattr = newattr
                    End If
                End If

            Loop

        End If

    Next

    Call term_setattr(oldattr)

    If Not frmTelnet.Receiving Then
        If oldcur = True Then
            Call term_Caretshow
        End If
    End If

    '<EhFooter>
    Exit Sub
term_redrawscreen_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_redrawscreen"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_scroll_up()
    '<EhHeader>
    On Error GoTo term_scroll_up_Err
    '</EhHeader>
    Dim i As Integer
    Dim s As Integer

    If frmTelnet.WindowState <> 1 Then
        ScrollDC frmTelnet.hdc, 0, -charheight, lprcScroll, lprcClip, hRgnUpdate, lprcUpdate
        TextOut frmTelnet.hdc, 0, CurY * charheight, Blank_Line, CharsPerLine
    End If

    'Update the redisplay buffer (only update the scrollable region)
    'Might consider making this a circular array so only one line
    'needs to be written per scroll, rather than relinking the array
    s = (lprcScroll.Top \ charheight + 1)

    For i = s To LastLine
        ScrImage(i) = ScrImage(i + 1)
        ScrAttr(i) = ScrAttr(i + 1)
    Next

    ScrImage(LinesPerPage) = Blank_Line
    ScrAttr(LinesPerPage) = Norm_Attr
    '<EhFooter>
    Exit Sub
term_scroll_up_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_scroll_up"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_setattr(CH As String)
    '<EhHeader>
    On Error GoTo term_setattr_Err

    '</EhHeader>
    With frmTelnet

        Select Case Asc(CH)

            Case 0  '   Normal
                ' Attr_BitMap = Attr_Norm
                .FontUnderline = False
                .FontItalic = False
                .FontBold = False
                SetTextColor .hdc, TermTextColor
                SetBkColor .hdc, TermBkColor

            Case 1  '   Bold
                ' Attr_BitMap = Attr_BitMap And Attr_Norm
                .FontBold = True

                '                SetTextColor(frmTelnet.hdc, QBColor(9))
            Case 5  '   Blinking
                ' Attr_BitMap = Attr_BitMap And Attr_Blink
                .FontItalic = True

                '                SetTextColor(frmTelnet.hdc, QBColor(3))
            Case 4  '   Underscore
                ' Attr_BitMap = Attr_BitMap And Attr_Under
                .FontUnderline = True

            Case 7  '   Reverse Video
                ' Attr_BitMap = Attr_BitMap And ATTR_REVERSE
                SetTextColor .hdc, TermBkColor
                SetBkColor .hdc, TermTextColor

            Case 8  '   Cancel (Invisible)
                'Attr_BitMap = Attr_BitMap And ATTR_INVISIBLE
                SetTextColor .hdc, TermBkColor
                SetBkColor .hdc, TermBkColor

                '===============================================================
            Case 30 '   Black Foreground
                SetTextColor .hdc, QBColor(0)

            Case 31 '   Red Foreground
                SetTextColor .hdc, QBColor(4)

            Case 32 '   Green Foreground
                SetTextColor .hdc, QBColor(2)

            Case 33 '   Yellow Foreground
                SetTextColor .hdc, QBColor(14)

            Case 34 '   Blue Foreground
                SetTextColor .hdc, QBColor(1)

            Case 35 '   Magenta Foreground
                SetTextColor .hdc, QBColor(5)

            Case 36 '   Cyan Foreground
                SetTextColor .hdc, QBColor(3)

            Case 37 '   White Foreground
                SetTextColor .hdc, QBColor(15)

                '===============================================================
            Case 40 '   Black Background
                SetBkColor .hdc, QBColor(0)

            Case 41 '   Red Background
                SetBkColor .hdc, QBColor(4)

            Case 42 '   Green Background
                SetBkColor .hdc, QBColor(2)

            Case 43 '   Yellow Background
                SetBkColor .hdc, QBColor(14)

            Case 44 '   Blue Background
                SetBkColor .hdc, QBColor(1)

            Case 45 '   Magenta Background
                SetBkColor .hdc, QBColor(5)

            Case 46 '   Cyan Background
                SetBkColor .hdc, QBColor(3)

            Case 47 '   White Background
                SetBkColor .hdc, QBColor(15)

            Case Else
                Exit Sub
        End Select

    End With

    curattr = CH
    '<EhFooter>
    Exit Sub
term_setattr_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_setattr"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_validatecurX()
    '<EhHeader>
    On Error GoTo term_validatecurX_Err

    '</EhHeader>
    If (CurX < 0) Then
        CurX = 0
    ElseIf CurX > LastChar Then
        CurX = LastChar
    End If

    '<EhFooter>
    Exit Sub
term_validatecurX_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_validatecurX"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_validatecurY()
    '<EhHeader>
    On Error GoTo term_validatecurY_Err

    '</EhHeader>
    If (CurY < 0) Then
        CurY = 0
    ElseIf CurY > LastLine Then
        CurY = LastLine
    End If

    '<EhFooter>
    Exit Sub
term_validatecurY_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_validatecurY"
    Resume Next
    '</EhFooter>
End Sub

Private Sub term_write(CH As Byte)
    '<EhHeader>
    On Error GoTo term_write_Err

    '</EhHeader>
    If frmTelnet.WindowState <> 1 Then TextOut frmTelnet.hdc, CurX * charWidth, CurY * charheight, Chr$(CH), 1
    If Not (CurX = LastChar) Then
        CurX = CurX + 1
    End If

    '<EhFooter>
    Exit Sub
term_write_Err:
    Controlar_Error Erl, Err.Description, "Reseter.vt100.term_write"
    Resume Next
    '</EhFooter>
End Sub
