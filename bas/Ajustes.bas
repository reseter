Attribute VB_Name = "Ajustes"
Option Explicit
'   Este archivo es parte del programa "reseter", el c�al es pertenece a SVCommunity.org y a Todosv.com
'   Mantenedores principales:
'    *Vlad
'Las siguientes constantes se exponen en esta secci�n para facilitar su cambio
Public Const PaginaIP As String = "http://dynupdate.no-ip.com/ip.php" 'Direcci�n de pagina que entrega la IP
Public Const EstadoVer As String = "Beta 4" 'Sub-Estado del programa
Public Const Intentos_Maximos As Byte = 2 'Numero m�ximo de intentos de reseteo sin exito - Base 0
Public Const Umbral_EsperarInternetMax As Double = 300000  'Tiempo m�ximo a esperar por internet (mSec)
Public Const Umbral_ChequeoInternet = 20000   'Intervalo entre chequeos de internet
Public Const Umbral_Reintento = 30000 'Intervalo entre intentos de reseteo fallidos
Public Umbral_Desconexion As Double
'Las siguientes variables se exponen en esta secci�n para facilitar su ubicaci�n
Public rINI_OPCIONES As String       'Ruta del INI de configuraci�n general
Public rINI_ROUTERS As String   'Ruta del INI con configuraci�n de dispositivos
Public Quieto As Boolean
Public NoIE As Boolean
'*****************************************************************************
'+Wine
'Forzar suposici�n de desconexi�n + NoIE = True
Public ModoLinux As Boolean

'*****************************************************************************
Public Function LeerINI(nSeccion As String, _
                        nClave As String, _
                        Optional nValor As String = vbNullString) As String
    '<EhHeader>
    On Error GoTo LeerINI_Err
    '</EhHeader>
    Dim Buffer As String * 32767
    Dim Lgt As Long
    Buffer = String$(32767, vbNullChar)
    Lgt = GetPrivateProfileString(nSeccion, nClave, nValor, Buffer, Len(Buffer), rINI_OPCIONES)

    If Lgt Then LeerINI = Left$(Buffer, Lgt) Else LeerINI = vbNullString
    '<EhFooter>
    Exit Function
LeerINI_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Ajustes.LeerINI"
    Resume Next
    '</EhFooter>
End Function

Public Sub EscribirINI(nSeccion As String, _
                       nClave As String, _
                       ByVal nValor As String)
    '<EhHeader>
    On Error GoTo EscribirINI_Err
    '</EhHeader>
    WritePrivateProfileString nSeccion, nClave, nValor, rINI_OPCIONES
    '<EhFooter>
    Exit Sub
EscribirINI_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Ajustes.EscribirINI"
    Resume Next
    '</EhFooter>
End Sub
