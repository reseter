Attribute VB_Name = "modBasico"
Option Explicit
' http://support.microsoft.com/?scid=kb%3Ben-us%3B231298&x=6&y=17
Private Type FILETIME
    dwLowDateTime As Long
    dwHighDateTime As Long
End Type
Private Const WAIT_OBJECT_0& = 0
Private Const INFINITE = &HFFFF
Private Const ERROR_ALREADY_EXISTS = 183&
Private Const QS_HOTKEY& = &H80
Private Const QS_KEY& = &H1
Private Const QS_MOUSEBUTTON& = &H4
Private Const QS_MOUSEMOVE& = &H2
Private Const QS_PAINT& = &H20
Private Const QS_POSTMESSAGE& = &H8
Private Const QS_SENDMESSAGE& = &H40
Private Const QS_TIMER& = &H10
Private Const QS_ALLINPUT& = (QS_SENDMESSAGE Or QS_PAINT Or QS_TIMER Or QS_POSTMESSAGE Or QS_MOUSEBUTTON Or QS_MOUSEMOVE Or QS_HOTKEY Or QS_KEY)
Private Declare Function CreateWaitableTimer _
                Lib "kernel32" _
                Alias "CreateWaitableTimerA" (ByVal lpSemaphoreAttributes As Long, _
                                              ByVal bManualReset As Long, _
                                              ByVal lpName As String) As Long
Private Declare Function SetWaitableTimer _
                Lib "kernel32" (ByVal hTimer As Long, _
                                lpDueTime As FILETIME, _
                                ByVal lPeriod As Long, _
                                ByVal pfnCompletionRoutine As Long, _
                                ByVal lpArgToCompletionRoutine As Long, _
                                ByVal fResume As Long) As Long
Private Declare Function CloseHandle _
                Lib "kernel32" (ByVal hObject As Long) As Long
Private Declare Function MsgWaitForMultipleObjects _
                Lib "user32" (ByVal nCount As Long, _
                              pHandles As Long, _
                              ByVal fWaitAll As Long, _
                              ByVal dwMilliseconds As Long, _
                              ByVal dwWakeMask As Long) As Long
Public Declare Function ShellExecute _
               Lib "shell32.dll" _
               Alias "ShellExecuteA" (ByVal hWnd As Long, _
                                      ByVal lpOperation As String, _
                                      ByVal lpFile As String, _
                                      ByVal lpParameters As String, _
                                      ByVal lpDirectory As String, _
                                      ByVal nShowCmd As Long) As Long
'Tomado de http://abstractvb.com/code.asp?A=939
Public Declare Function SetTimer _
               Lib "user32" (ByVal hWnd As Long, _
                             ByVal nIDEvent As Long, _
                             ByVal uElapse As Long, _
                             ByVal lpTimerFunc As Long) As Long
Public Declare Function KillTimer _
               Lib "user32" (ByVal hWnd As Long, _
                             ByVal nIDEvent As Long) As Long

Public Sub Esperar(lNumberOfSeconds As Single)
    '<EhHeader>
    On Error GoTo Esperar_Err
    '</EhHeader>
    Dim ft As FILETIME
    Dim lBusy As Long
    Dim dblDelay As Double
    Dim dblDelayLow As Double
    Dim dblUnits As Double
    Dim hTimer As Long
    hTimer = CreateWaitableTimer(0, True, App.EXEName & "Timer")

    If Err.LastDllError <> ERROR_ALREADY_EXISTS Then
        ft.dwLowDateTime = -1
        ft.dwHighDateTime = -1
        SetWaitableTimer hTimer, ft, 0, 0, 0, 0
    End If

    dblUnits = CDbl(&H10000) * CDbl(&H10000)
    dblDelay = CDbl(lNumberOfSeconds) * 1000 * 10000
    ft.dwHighDateTime = -CLng(dblDelay / dblUnits) - 1
    dblDelayLow = -dblUnits * (dblDelay / dblUnits - Fix(dblDelay / dblUnits))

    If dblDelayLow < CDbl(&H80000000) Then
        dblDelayLow = dblUnits + dblDelayLow
        ft.dwHighDateTime = ft.dwHighDateTime + 1
    End If

    ft.dwLowDateTime = CLng(dblDelayLow)
    SetWaitableTimer hTimer, ft, 0, 0, 0, False

    Do
        lBusy = MsgWaitForMultipleObjects(1, hTimer, False, INFINITE, QS_ALLINPUT&)

        DoEvents
    Loop Until lBusy = WAIT_OBJECT_0

    ' Close the handles when you are done with them.
    CloseHandle hTimer
    '<EhFooter>
    Exit Sub
Esperar_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modBasico.Esperar"
    Resume Next
    '</EhFooter>
End Sub
