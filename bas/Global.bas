Attribute VB_Name = "Global"
Option Explicit
'   "Reseter", Copyright 2006-2007 TodoSV.com
'   Licencia adicional a: www.svcommunity.org y www.untercio.net
'   Mantenedores principales:
'    *Vlad
'    *Kikeuntercio
'   Para soporte, dudas, consultas, inquitudes y dem�s:
'   http://foro.todosv.com/reseter/
'   http://www.svcommunity.org/forum/index.php?topic=25095.0
'   http://www.svcommunity.org/forum/index.php?topic=59743.0
Private TiempoMax As Double
'Tipo
Enum e_Acceso
    web
    telnet
    auro
End Enum
Enum e_AccionEX
    ed_clic
    ed_java
    ed_llenar
    ed_navegar
End Enum
Type t_Datos
    Direccion As String         'Direcci�n a navegar
    usuario As String           'Usuario del router
    clave As String             'Clave del usuario del router
    base As String              'Direcci�n de la puerta de enlace
    puerto As Long              'Puerto de acceso
    nForm As Integer
    nCont As Integer
    accionTipo As e_AccionEX
    accionEX As String
    accionEX2 As String
    codigo As String            'Lista de codigos a ejecutarse
    tipoAcceso As e_Acceso      'Tipo de acceso [Telnet | Web]
    renovar As Boolean          'Ejecutar renovaci�n de IP
    asumirDes As Boolean        'Asumir desconexion o probar?
End Type
Public m_Datos As t_Datos
Public flag_Navegar As Boolean
Public IE As Object
Public TelnetComandos() As String
Public nComando As Integer
Public NetError As Boolean
Private Intentos_Realizados As Byte
Public pSocket As AuroNet
Public ppSocket As AuroNet
Public Tiempo As cTimer         'Uso general
Public TiempoEx As cTimer       'Uso secundario
Public TiempoEx2 As New cTimer  'Uso terciario
Public dXMR As String
Public dXMR_ok As Boolean

Public Sub Main()
    'Activar_Temas_XP
    'Rutas de archivos
    '------------------------
    '<EhHeader>
    On Error GoTo Main_Err
    '</EhHeader>
    Set Tiempo = New cTimer
    Set TiempoEx = New cTimer
    'Empezamos a medir el tiempo que tarda en iniciar el programa
    Tiempo.StartTimer
    '------------------------
    'Iniciamos los Sockets
    Set pSocket = New AuroNet
    Set ppSocket = New AuroNet
    pSocket.Direccion = PaginaIP
    ppSocket.Direccion = "www.google.com"
    '------------------------
    EthInfo
    '------------------------
    'Fichero INI con las opciones del programa
    rINI_OPCIONES = App.Path & "\Reseter.ini"
    'Fichero INI con los c�digos de reseteo para los routers
    rINI_ROUTERS = App.Path & "\Routers.ini"
    '------------------------
    Quieto = LeerINI("Opciones", "Quieto", False)
    'Ser� que es mejor no iniciar el Objeto IE?
    'Esto es util cuando se ha desinstalado el Explorer o en Wine
    NoIE = LeerINI("Opciones", "NoIE", False)
    ModoLinux = LeerINI("Opciones", "Linux:Compatibilidad", False)
    '------------------------
    frmPrincipal.Show

    DoEvents
    '------------------------
    Registrar "Iniciando Reseter: " & App.Major & "." & App.Minor & "." & App.Revision & " " & EstadoVer
    Registrar "Propiedad de todosv.com, svcommunity.org y untercio.net"
    Registrar "Informaci�n: http://foro.todosv.com/reseter/"
    Registrar "IP p�blica gracias a " & ObtenerDominio(PaginaIP)

    '------------------------
    'Objeto IE
    If NoIE Then
        Registrar "+Objeto IE: NoIE establecido."
    Else
        Crear_Objeto_IE
    End If

    Registrar "+IP Local: " & pSocket.IpLocal
    Registrar "+IP Gateway: " & EthGateWay
    Cambio_IP
    Obtener_Lista_Externa
    frmPrincipal.ComReset.Text = Predefinido
    'Comprobando XMR
    '------------------------
    dXMR = App.Path & "\XMR.exe"
    dXMR_ok = (Dir$(dXMR) = vbNullString)
    Registrar "�XMR?... " & IIf(dXMR_ok, "�Si!", "No...")
    '------------------------
    'Interfaz
    '------------------------
    Registrar "Reestableciendo interfaz"
    frmPrincipal.chkAvanzado.Value = LeerINI("Interfaz", "MAvanzado", Unchecked)
    frmPrincipal.chkTerminal.Value = LeerINI("Interfaz", "MTerminal", Unchecked)
    frmPrincipal.txtNumero.Text = LeerINI("Opciones", "Telefono", vbNullString)
    'variables
    Umbral_Desconexion = 30000

    '------------------------
    'Verificar si hay router predefinido y proceder
    '------------------------
    If HayPredefinido = True Then
        Registrar "Router predefinido, 10segs. para reseteo o cancelar"
        frmPrincipal.cmdPredefinir.Caption = "Cancelar"
        frmPrincipal.cmdReset.Enabled = False
        frmPrincipal.ComReset.Enabled = False
        TiempoEx.StartTimer
        TiempoEx2.StartTimer

        'Esperamos los 10segs � que la persona presione cancelar
        Do Until TiempoEx.Elapsed > 10000 Or NetError = True
            Esperar 0.1
            frmPrincipal.lblTW.Caption = Fix((10000 - TiempoEx.Elapsed) / 1000)

            If TiempoEx2.Elapsed > 1000 Then
                Beep
                TiempoEx2.StartTimer
            End If

        Loop

        TiempoEx2.EndTimer

        'Si no presion� cancelar
        If Not NetError Then
            Registrar "{[PRE]:Iniciando reseteo predefinido}"
            Procesar_Codigo
            Registrar "{[PRE]:Terminando programa}"
            GuardarRegistro
            Terminar
        Else
            'Si presion� cancelar
            Registrar "Se ha cancelado el reseteo automatico"
            NetError = False
        End If
    End If

    TiempoEx.EndTimer
    frmPrincipal.cmdReset.Enabled = True
    frmPrincipal.ComReset.Enabled = True
    Tiempo.EndTimer
    '------------------------
    Registrar "Reseter tard� " & Round(Tiempo.Elapsed / 1000, 2) & "segs. en cargar"
    '<EhFooter>
    Exit Sub
Main_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Global.Main"
    Resume Next
    '</EhFooter>
End Sub

Public Sub Reiniciar()
    '<EhHeader>
    On Error GoTo Reiniciar_Err
    '</EhHeader>
    Dim bIP As IP
    Dim Detectado As Boolean
    'Reseteamos las variables.
    NetError = False

    'Verificamos si existe el objeto IE y verificamos si debe o no
    'de existir.
    If IE Is Nothing And m_Datos.tipoAcceso = web Then
        If NoIE Then
            Registrar "!!!No hay objeto IE porque se especific� NoIE=-1"
        Else
            Registrar "!!!Por alguna causa el objeto IE no esta!"
        End If
    End If

    'Verificamos el tipo de operaci�n [Web | Telnet | AuroNet]
    'y la ejecutamos
    Select Case m_Datos.tipoAcceso

        Case Is = e_Acceso.web
            Registrar "+-Ser� un reseteo via WEB"
            res_Web

        Case Is = e_Acceso.telnet
            Registrar "+-Ser� un reseteo via Telnet"
            res_Telnet

        Case Is = e_Acceso.auro
            Registrar "+-Ser� un reseteo via WEB/AuroNet"
            res_auro
    End Select

    'Si ocurri� alg�n error en el reinicio
    If NetError = True Then
        Registrar "!!! Ha ocurrido un error critico y el reseteo no continuar�"
        Exit Sub
    End If

    'Esperamos la reconexi�n
    If m_Datos.asumirDes Or ModoLinux Then
        Registrar "+--Esperando " & Fix((Umbral_Desconexion / 1000)) & "Segs. para asumir des/conexi�n de router"
        Tiempo.StartTimer

        Do
            Esperar 0.5
            frmPrincipal.lblTW.Caption = Fix((Umbral_Desconexion - Tiempo.Elapsed) / 1000)
        Loop Until Tiempo.Elapsed > Umbral_Desconexion Or NetError Or GetNetConnectString = False

        Tiempo.EndTimer

        If NetError Then Exit Sub
    Else
        Registrar "+--Esperando " & Fix(Umbral_Desconexion / 1000) & " segundos para desconexi�n de router"
        Tiempo.StartTimer

        Do
            Esperar 0.5
            frmPrincipal.lblTW.Caption = Fix((Umbral_Desconexion - Tiempo.Elapsed) / 1000)
        Loop Until Tiempo.Elapsed > Umbral_Desconexion Or NetError Or GetNetConnectString = False

        Tiempo.EndTimer

        If NetError Or Tiempo.Elapsed > Umbral_Desconexion Then
            Registrar "+---El router nunca se desconect� o ocurri� otro error"
            Error_Cel
            NetError = True
            Exit Sub
        End If

        Registrar "+--Esperando " & Fix(Umbral_Desconexion / 1000) & "segundos para reconexi�n de router"
        Tiempo.StartTimer

        Do
            Esperar 0.5
            frmPrincipal.lblTW.Caption = Fix((Umbral_Desconexion - Tiempo.Elapsed) / 1000)
        Loop Until Tiempo.Elapsed > Umbral_Desconexion Or NetError Or GetNetConnectString = True

        Tiempo.EndTimer

        If NetError Or Tiempo.Elapsed > Umbral_Desconexion Then
            Registrar "+---El router nunca se conect� o ocurri� otro error"
            Error_Cel
            NetError = True
            Exit Sub
        End If
    End If

    'Esperar 5 minutos a que se recupere el internet
    Registrar "+--Esperando recuperaci�n de internet (m�ximo " & (Umbral_EsperarInternetMax / 60000) & "minutos)"
    Tiempo.StartTimer
    TiempoEx.StartTimer
    Dim dTemporizador As Long
    dTemporizador = SetTimer(0, 0, 1000, AddressOf lpActualizar)
    Detectado = HayInternet

    If Not Detectado Then

        Do Until (Tiempo.Elapsed > Umbral_EsperarInternetMax) Or NetError
            Esperar 0.5

            'Chequear si ya hay internet
            If TiempoEx.Elapsed > Umbral_ChequeoInternet Then
                Detectado = HayInternet

                If Detectado Then Exit Do
            End If

        Loop

    End If

    KillTimer 0, dTemporizador
    Tiempo.EndTimer
    TiempoEx.EndTimer

    If Detectado And NetError = False Then
        'Si hay internet entonces verificamos si la IP cambi�
        '1: Si no cambi� entonces reseteamos el router de nuevo
        '2: siempre y cuando no supere cierto limite de reintentos.
        bIP = Cambio_IP

        If bIP.Cambio = True Then
            Registrar "++La IP no cambi�, reintentando reseteo en " & (Umbral_Reintento / 1000) & "Segs."
            Intentos_Realizados = Intentos_Realizados + 1

            'Como no cambi� reintentamos el reseteo
            If Intentos_Realizados > Intentos_Maximos Then
                'Si ya no nos quedan intentos...
                Registrar "++-Se han hecho " & Intentos_Realizados & " intentos sin exito, deteniendo"
                Registrar "+Se han cancelado los reintentos a�tomaticos"
                Exit Sub
            Else
                'Si a�n nos quedan intentos...
                Tiempo.StartTimer
                'Esperar 30segs. para reintentar reseteo
                Registrar "+Esperando " & Fix(Umbral_Reintento / 1000) & " segundos para intento #" & Intentos_Realizados + 1

                Do
                    Esperar 0.5
                    frmPrincipal.lblTW.Caption = Fix((Umbral_Desconexion - Tiempo.Elapsed) / 1000)
                Loop Until Tiempo.Elapsed > Umbral_Reintento Or NetError

                If Not NetError Then
                    Procesar_Codigo
                    Exit Sub
                End If
            End If

        Else
            Registrar "+El cambi� fue satisfactorio"

            If dXMR_ok And frmPrincipal.txtNumero.Text <> vbNullString Then
                Registrar "Enviando mensaje de exito a usuario"
                XMR_Enviar frmPrincipal.txtNumero.Text, "El reseteo fue exitoso, a las " & Now & " | nueva IP: " & bIP.IP_Actual
            Else
                Registrar "No habia numero para notificacion de usuario"
            End If

            frmPrincipal.lblTW.Caption = "Exito"
        End If

    Else
        'Si no se detect� para nada el internet
        Registrar "+El tiempo de espera se agot� y no se detecto conexi�n."
        Registrar "++El servidor " & PaginaIP & " puede estar caido."
        Registrar "+Se han cancelado los reintentos a�tomaticos"
        Error_Cel
        frmPrincipal.lblTW.Caption = "Error"
    End If

    ' Reseteamos algunas variables
    Umbral_Desconexion = 30000
    '<EhFooter>
    Exit Sub
Reiniciar_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Global.Reiniciar"
    Resume Next
    '</EhFooter>
End Sub

Public Function GuardarRegistro() As String
    '<EhHeader>
    On Error GoTo GuardarRegistro_Err
    '</EhHeader>
    Dim i As Byte
    Dim Buffer As String
    Static ocupado As Boolean

    If ocupado Then Exit Function
    ocupado = True
    i = FreeFile
    Buffer = frmPrincipal.txtSalida.Text
    Open App.Path & "\" & Day(Now) & "-" & Month(Now) & ".txt" For Append Access Write As #i
    Print #i, CStr(Now)
    Print #i, CStr(Buffer)
    Close #i
    GuardarRegistro = (App.Path & "\" & Day(Now) & "-" & Month(Now) & ".txt")
    ocupado = False
    '<EhFooter>
    Exit Function
GuardarRegistro_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Global.GuardarRegistro"
    Resume Next
    '</EhFooter>
End Function

'CSEH: Skip
Public Sub Controlar_Error(linea As Long, _
                           descripcion As String, _
                           lugar As String)
100     Registrar "���""" & descripcion & """, erl #" & linea & " - """ & lugar & """"

102     If Quieto Then
104         GuardarRegistro
        Else

106         If MsgBox("Ocurri� el error """ & descripcion & """, en linea #" & linea & " de """ & lugar & """" & vbNewLine & "Por favor notifica de este error al tema especifico de reseter en svcommunity.org � todosv.com" & vbNewLine & "Se ha guardado un registro de error en """ & GuardarRegistro & """ por favor envielo a los creadores" & vbNewLine & "�Desea terminar la aplicaci�n?", vbCritical + vbYesNo) = vbYes Then End
        End If

End Sub

Public Sub Predefinir(Valor As Boolean)
    '<EhHeader>
    On Error GoTo Predefinir_Err
    '</EhHeader>
    EscribirINI "Opciones", "BPredefinido", CStr(CInt(Valor))
    EscribirINI "Opciones", "Predefinido", frmPrincipal.ComReset.Text
    '<EhFooter>
    Exit Sub
Predefinir_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Global.Predefinir"
    Resume Next
    '</EhFooter>
End Sub

Public Function Predefinido() As String
    '<EhHeader>
    On Error GoTo Predefinido_Err
    '</EhHeader>
    Predefinido = LeerINI("Opciones", "Predefinido", "Escoja su router/modem")
    '<EhFooter>
    Exit Function
Predefinido_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Global.Predefinido"
    Resume Next
    '</EhFooter>
End Function

Public Function ObtenerDominio(URL As String) As String
    '<EhHeader>
    On Error GoTo ObtenerDominio_Err
    '</EhHeader>
    Dim Buffer As Long
    Buffer = InStr(8, URL, "/")

    If Buffer > 0 Then ObtenerDominio = Replace$(Mid$(URL, 1, Buffer - 1), "http://", vbNullString) Else ObtenerDominio = URL
    '<EhFooter>
    Exit Function
ObtenerDominio_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Global.ObtenerDominio"
    Resume Next
    '</EhFooter>
End Function

Public Function HayPredefinido() As Boolean
    '<EhHeader>
    On Error GoTo HayPredefinido_Err
    '</EhHeader>
    HayPredefinido = LeerINI("Opciones", "BPredefinido", False)
    '<EhFooter>
    Exit Function
HayPredefinido_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Global.HayPredefinido"
    Resume Next
    '</EhFooter>
End Function

Public Sub XMR_Enviar(Numero As String, _
                      Mensaje As String)
    'Gracias a Olatin por la idea de enviar mensaje con notificaciones!.
    '<EhHeader>
    On Error GoTo XMR_Enviar_Err
    '</EhHeader>
    ShellExecute frmPrincipal.hWnd, "", dXMR & " /x n=" & Numero & "|m=" & Mensaje & "|f=Reseter 4.0", "", "", 0
    '<EhFooter>
    Exit Sub
XMR_Enviar_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Global.XMR_Enviar"
    Resume Next
    '</EhFooter>
End Sub

Public Sub Error_Cel()
    '<EhHeader>
    On Error GoTo Error_Cel_Err

    '</EhHeader>
    If dXMR_ok And frmPrincipal.txtNumero.Text <> vbNullString Then
        Registrar "Enviando mensaje de error a usuario"
        XMR_Enviar frmPrincipal.txtNumero.Text, "El reseteo NO fue exitoso, fallo ocurrido en " & Now
    End If

    '<EhFooter>
    Exit Sub
Error_Cel_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Global.Error_Cel"
    Resume Next
    '</EhFooter>
End Sub

Public Sub Procesar_Codigo()
    'Primero quebramos el codigo en un array, donde cada elemento contendr� una linea de codigo b�sico.
    '<EhHeader>
    On Error GoTo Procesar_Codigo_Err
    '</EhHeader>
    Dim A_Codigo() As String
    Dim i As Long
    Dim cTimeEspera As New cTimer
    A_Codigo = Split(m_Datos.codigo, Chr(254))

    For i = 0 To UBound(A_Codigo)

        'Saltar los comentarios
        If Left(A_Codigo(i), 2) = "//" Then Registrar "PreProcesador: Comentario: '" & Mid$(A_Codigo(i), 3) & "'"
        'Reemplazar variables
        A_Codigo(i) = Replace$(A_Codigo(i), "$$GATEWAY$$", EthGateWay)

        '#########################################################################################
        '                              Ejecutores
        '#########################################################################################
        'El codigo dice que tenemos que reiniciar?, bien, "tratemos"
        If A_Codigo(i) = "objetivo.reiniciar" Then
            Registrar "PreProcesador: 'objetivo.reiniciar' ejecutando 'Reiniciar'"
            Reiniciar
            Registrar "PreProcesador: 'objetivo.reiniciar' ejecutado."
        End If

        'El codigo dice que tenemos que ejecutemos el procesador Res_web?
        If A_Codigo(i) = "web.procesar" Then
            Registrar "PreProcesador: 'web.procesar' ejecutando 'Res_Web'"
            res_Web
            Registrar "PreProcesador: 'web.procesar' ejecutado."
        End If

        'El codigo diceque tenemos que esperar X segundos antes de seguir?
        If InStr(1, A_Codigo(i), "mi.esperar=") Then
            TiempoMax = CDbl(Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1)) * 1000
            Registrar "PreProcesador: 'mi.esperar' iniciada [" & TiempoMax & "]"
            cTimeEspera.StartTimer

            Do
                Esperar 0.5
            Loop Until cTimeEspera.Elapsed > TiempoMax

            Registrar "PreProcesador: comando 'mi.esperar' concluido"
        End If

        '#########################################################################################
        '#########################################################################################
        '#########################################################################################
        '                              Banderas
        '#########################################################################################
        'El codigo dice que tenemos que hacer clic en la direccion indicada?
        If A_Codigo(i) = "web.accion.clic" Then
            m_Datos.accionTipo = ed_clic
            Registrar "PreProcesador: 'web.accion.clic' traducido a 'm_Datos.accionTipo=ed_clic'"
        End If

        'El codigo dice que solo tenemos que navegar la direccion indicada? (codigo JAVA)
        If A_Codigo(i) = "web.accion.java" Then
            m_Datos.accionTipo = ed_java
            Registrar "PreProcesador: 'web.accion.java' traducido a 'm_Datos.accionTipo=ed_java'"
        End If

        'El codigo dice que solo tenemos que navegar la direccion indicada?
        If A_Codigo(i) = "web.accion.navegar" Then
            m_Datos.accionTipo = ed_navegar
            Registrar "PreProcesador: 'web.accion.navegar' traducido a 'm_Datos.accionTipo=ed_navegar'"
        End If

        'El codigo dice que tenemos que llenar informaci�n en la direccion y posici�n indicada?
        If InStr(1, A_Codigo(i), "web.accion.llenar") Then
            m_Datos.accionTipo = ed_llenar
            Registrar "PreProcesador: 'web.accion.llenar' traducido a 'm_Datos.accionTipo=ed_llenar'"
        End If

        'Debemos asumir que el router se desconectar� internamente?.
        If InStr(1, A_Codigo(i), "objetivo.asumir.desconexion=") Then
            m_Datos.asumirDes = CBool(Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1))
            Registrar "PreProcesador: 'objetivo.asumir.desconexion' traducido a 'm_Datos.asumirDes=" & m_Datos.asumirDes & "'"
        End If

        'El codigo dice que tenemos que renovar despues de la operaci�n?
        If InStr(1, A_Codigo(i), "red.renovar=") Then
            m_Datos.renovar = CBool(Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1))
            Registrar "PreProcesador: 'red.renovar' traducido a 'm_Datos.renovar=" & m_Datos.renovar & "'"
        End If

        'El codigo dice que no tenemos que navegar en reiniciar.res_web y en web.procesar?
        If InStr(1, A_Codigo(i), "web.navegar=") Then
            flag_Navegar = CBool(Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1))
            Registrar "PreProcesador: 'web.navegar' traducido a 'flag_Navegar=" & flag_Navegar & "'"
        End If

        'El codigo dice que tenemos que cambiar el limite para asumir desconexion -temporal-?
        'mi.umbral.desconexion=<TIEMPO EN SEGUNDOS>
        If InStr(1, A_Codigo(i), "mi.umbral.desconexion=") Then
            Umbral_Desconexion = Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1) * 1000
            Registrar "PreProcesador: 'mi.umbral.desconexion' traducido a 'Umbral_Desconexion=" & Umbral_Desconexion & "'"
        End If

        '#########################################################################################
        '#########################################################################################
        '#########################################################################################
        '                              Ajustes de parametros
        '#########################################################################################
        'El codigo dice que tenemos que ajustar las opciones para procesar telnet?
        'telnet=<COMANDOS SEPARADOS POR ";">
        If InStr(1, A_Codigo(i), "telnet.comando=") Then
            m_Datos.tipoAcceso = telnet
            m_Datos.accionEX = Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1)
            Registrar "PreProcesador: 'telnet' traducido a 'm_Datos.accionEX=" & m_Datos.accionEX & "'"
        End If

        'El codigo dice que tenemos que ajustar las opciones para procesar web?
        If InStr(1, A_Codigo(i), "web.url=") Then
            m_Datos.accionEX = Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1)
            m_Datos.tipoAcceso = web
            Registrar "PreProcesador: 'web.url' traducido a 'm_Datos.accionEX=" & m_Datos.accionEX & "'"
        End If

        'El codigo nos da datos para EX2
        If InStr(1, A_Codigo(i), "web.datos=") Then
            m_Datos.accionEX2 = Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1)
            Registrar "PreProcesador: 'web.datos' traducido a 'm_Datos.accionEX2=" & m_Datos.accionEX2 & "'"
        End If

        'El codigo especifica el puerto a usar?
        If InStr(1, A_Codigo(i), "puerto=") Then
            m_Datos.puerto = Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1)
            Registrar "PreProcesador: 'puerto' traducido a 'm_Datos.puerto=" & m_Datos.puerto & "'"
        End If

        'El codigo especifica el control a usar?
        If InStr(1, A_Codigo(i), "web.control=") Then
            m_Datos.nCont = Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1)
            Registrar "PreProcesador: 'web.control' traducido a 'm_Datos.nCont=" & m_Datos.nCont & "'"
        End If

        'El codigo especifica el formulario a usar?
        If InStr(1, A_Codigo(i), "web.formulario=") Then
            m_Datos.nForm = Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1)
            Registrar "PreProcesador: 'web.formulario' traducido a 'm_Datos.nForm=" & m_Datos.nForm & "'"
        End If

        'El codigo especifica el usuario a usar?
        If InStr(1, A_Codigo(i), "web.usuario=") Then
            m_Datos.usuario = Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1)
            Registrar "PreProcesador: 'web.usuario' traducido a 'm_Datos.usuario=" & m_Datos.usuario & "'"
        End If

        'El codigo especifica la clave a usar?
        If InStr(1, A_Codigo(i), "web.clave=") Then
            m_Datos.clave = Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1)
            Registrar "PreProcesador: 'clave' traducido a 'm_Datos.clave=" & m_Datos.clave & "'"
        End If

        'El codigo especifica la direcci�n base a usar?
        If InStr(1, A_Codigo(i), "objetivo.ip=") Then
            m_Datos.base = Mid$(A_Codigo(i), InStr(1, A_Codigo(i), "=") + 1)
            Registrar "PreProcesador: 'base' traducido a 'm_Datos.base=" & m_Datos.base & "'"
        End If

        '#########################################################################################
        '#########################################################################################
    Next

    '<EhFooter>
    Exit Sub
Procesar_Codigo_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Global.Procesar_Codigo"
    Resume Next
    '</EhFooter>
End Sub

Public Function HayInternet() As Boolean
    '<EhHeader>
    On Error GoTo HayInternet_Err

    '</EhHeader>
    If m_Datos.renovar = True Then RenovarLAN
    Registrar "+---Chequeando conexi�n a internet... espere"
    Esperar 0.5

    If ppSocket.HTML_CONSULTAR Then
        Registrar "+----Chequeo -> Se detect� internet"
        HayInternet = True
    Else
        Registrar "+----Chequeo -> A�n no hay internet"
        TiempoEx.StartTimer
    End If

    '<EhFooter>
    Exit Function
HayInternet_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Global.HayInternet"
    Resume Next
    '</EhFooter>
End Function

Public Sub Restablecer_Todo()
    '<EhHeader>
    On Error GoTo Restablecer_Todo_Err
    '</EhHeader>
    flag_Navegar = True
    TelnetComandos = Split("")
    nComando = 0
    NetError = False
    Intentos_Realizados = 0

    With m_Datos
        .accionEX = ""
        .accionEX2 = ""
        .accionTipo = ed_clic
        .asumirDes = False
        .base = ""
        .clave = ""
        .codigo = ""
        .Direccion = ""
        .nCont = -1
        .nForm = -1
        .puerto = 0
        .renovar = True
        .tipoAcceso = web
        .usuario = ""
    End With

    '<EhFooter>
    Exit Sub
Restablecer_Todo_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Global.Restablecer_Todo"
    Resume Next
    '</EhFooter>
End Sub

Public Sub Terminar()
    '<EhHeader>
    On Error GoTo Terminar_Err
    '</EhHeader>
    Destruir_Conexion
    EscribirINI "Opciones", "Quieto", CStr(CInt(Quieto))
    EscribirINI "Opciones", "NoIE", CStr(CInt(NoIE))
    EscribirINI "Opciones", "Telefono", CStr(frmPrincipal.txtNumero.Text)
    EscribirINI "Opciones", "Linux:Compatibilidad", CStr(CInt(ModoLinux))
    Set pSocket = Nothing
    Set ppSocket = Nothing
    Dim f As Form

    For Each f In Forms
        Unload f
    Next

    End
    '<EhFooter>
    Exit Sub
Terminar_Err:
    Controlar_Error Erl, Err.Description, "Reseter.Global.Terminar"
    Resume Next
    '</EhFooter>
End Sub
