Attribute VB_Name = "ConfIO"
Option Explicit
Public Declare Function GetPrivateProfileString _
               Lib "kernel32" _
               Alias "GetPrivateProfileStringA" (ByVal lpSectionName As String, _
                                                 ByVal lpKeyName As Any, _
                                                 ByVal lpDefault As String, _
                                                 ByVal lpbuffurnedString As String, _
                                                 ByVal nBuffSize As Long, _
                                                 ByVal lpFileName As String) As Long
Public Declare Function WritePrivateProfileString _
               Lib "kernel32" _
               Alias "WritePrivateProfileStringA" (ByVal lpSectionName As String, _
                                                   ByVal lpKeyName As Any, _
                                                   ByVal lpString As Any, _
                                                   ByVal lpFileName As String) As Long
Private Declare Function GetPrivateProfileSectionNames _
                Lib "kernel32.dll" _
                Alias "GetPrivateProfileSectionNamesA" (ByVal lpszReturnBuffer As String, _
                                                        ByVal nSize As Long, _
                                                        ByVal lpFileName As String) As Long
Private Declare Function GetPrivateProfileSection _
                Lib "kernel32" _
                Alias "GetPrivateProfileSectionA" (ByVal lpAppName As String, _
                                                   ByVal lpReturnedString As String, _
                                                   ByVal nSize As Long, _
                                                   ByVal lpFileName As String) As Long
'Las siguientes constantes determinan el nombre de la clave a ocupar para el parametro
Private Const IO_codigo As String = "Codigo"

Public Sub Obtener_Lista_Externa()
    'Lista
    '<EhHeader>
    On Error GoTo Obtener_Lista_Externa_Err
    '</EhHeader>
    Dim Modelos() As String
    Dim szBuf As String, lLen As Integer
    'Registrar "Cargando perfiles de dispositivos soportados..."
    'Obtenemos los nombres de todas las secciones, vienen separadas por NullChar Chr(0)
    szBuf = String$(32767, vbNullChar) 'Creamos el buffer
    lLen = GetPrivateProfileSectionNames(szBuf, Len(szBuf), rINI_ROUTERS) 'Obtenemos el largo del la lectura
    szBuf = Left$(szBuf, lLen) 'Cortamos lo innecesario

    If szBuf = vbNullString Then
        Exit Sub
    End If

    Modelos = Split(szBuf, vbNullChar)
    ReDim Preserve Modelos(UBound(Modelos) - 1) As String
    Dim i As Long 'Para el contador
    Dim a As Long 'Total de modelos
    a = UBound(Modelos)

    For i = 0 To a
        frmPrincipal.ComReset.AddItem Modelos(i)
    Next

    Registrar "+" & a & " perfiles cargados"
    '<EhFooter>
    Exit Sub
Obtener_Lista_Externa_Err:
    Controlar_Error Erl, Err.Description, "Reseter.ConfIO.Obtener_Lista_Externa"
    Resume Next
    '</EhFooter>
End Sub

Public Sub Memoria_IO(nRouter As String)
    'De la memoria al archivo
    '<EhHeader>
    On Error GoTo Memoria_IO_Err

    '</EhHeader>
    With frmPrincipal
        Escribir nRouter, IO_codigo, m_Datos.codigo
    End With

    '<EhFooter>
    Exit Sub
Memoria_IO_Err:
    Controlar_Error Erl, Err.Description, "Reseter.ConfIO.Memoria_IO"
    Resume Next
    '</EhFooter>
End Sub

Public Sub IO_Memoria(nRouter As String)
    'Del archivo a la memoria
    '<EhHeader>
    On Error GoTo IO_Memoria_Err

    '</EhHeader>
    With frmPrincipal
        m_Datos.codigo = Leer(nRouter, IO_codigo)
    End With

    Mostrar_Datos
    '<EhFooter>
    Exit Sub
IO_Memoria_Err:
    Controlar_Error Erl, Err.Description, "Reseter.ConfIO.IO_Memoria"
    Resume Next
    '</EhFooter>
End Sub

Private Function Leer(nRouter As String, _
                      nEspecificacion As String, _
                      Optional nValor As String = vbNullString) As String
    '<EhHeader>
    On Error GoTo Leer_Err
    '</EhHeader>
    Dim Buffer As String * 32767
    Dim Lgt As Long
    Buffer = String$(32767, vbNullChar)
    Lgt = GetPrivateProfileString(nRouter, nEspecificacion, nValor, Buffer, Len(Buffer), rINI_ROUTERS)

    If Lgt Then Leer = Left$(Buffer, Lgt) Else Leer = vbNullString
    '<EhFooter>
    Exit Function
Leer_Err:
    Controlar_Error Erl, Err.Description, "Reseter.ConfIO.Leer"
    Resume Next
    '</EhFooter>
End Function

Private Sub Escribir(nRouter As String, _
                     nEspecificacion As String, _
                     ByVal nValor As String)
    '<EhHeader>
    On Error GoTo Escribir_Err
    '</EhHeader>
    WritePrivateProfileString nRouter, nEspecificacion, nValor, rINI_ROUTERS
    '<EhFooter>
    Exit Sub
Escribir_Err:
    Controlar_Error Erl, Err.Description, "Reseter.ConfIO.Escribir"
    Resume Next
    '</EhFooter>
End Sub

Public Function Existe_Seccion(nRouter As String) As Boolean
    '28/04/07 -> nRouter definido como String en lugar de Variant
    '<EhHeader>
    On Error GoTo Existe_Seccion_Err
    '</EhHeader>
    Dim Buffer As String
    Buffer = String$(32767, Chr$(0))
    Existe_Seccion = GetPrivateProfileSection(nRouter, Buffer, Len(Buffer), rINI_ROUTERS)
    '<EhFooter>
    Exit Function
Existe_Seccion_Err:
    Controlar_Error Erl, Err.Description, "Reseter.ConfIO.Existe_Seccion"
    Resume Next
    '</EhFooter>
End Function
