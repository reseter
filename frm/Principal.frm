VERSION 5.00
Begin VB.Form frmPrincipal 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   " Reseter|svcommunity.org|todosv.com|untercio.net"
   ClientHeight    =   3675
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5760
   Icon            =   "Principal.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3675
   ScaleWidth      =   5760
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame frmXMR 
      Caption         =   "Integraci�n con XMR"
      Height          =   765
      Left            =   105
      TabIndex        =   12
      Top             =   8445
      Width           =   5535
      Begin VB.TextBox txtNumero 
         Height          =   330
         Left            =   3750
         TabIndex        =   14
         Top             =   300
         Width           =   1590
      End
      Begin VB.Label Label1 
         Caption         =   "N�mero telef�nico de notificaci�n:"
         Height          =   225
         Left            =   105
         TabIndex        =   13
         Tag             =   "1"
         Top             =   375
         Width           =   3495
      End
   End
   Begin VB.CheckBox chkAvanzado 
      Alignment       =   1  'Right Justify
      Caption         =   "Mostrar Avanzado"
      Height          =   435
      Left            =   1485
      TabIndex        =   11
      Tag             =   "1"
      Top             =   3180
      Width           =   2280
   End
   Begin VB.CommandButton cmdPredefinir 
      Caption         =   "Predefinir"
      Enabled         =   0   'False
      Height          =   360
      Left            =   4043
      TabIndex        =   10
      Tag             =   "1"
      Top             =   75
      Width           =   1620
   End
   Begin VB.CheckBox chkTerminal 
      Alignment       =   1  'Right Justify
      Caption         =   "Mostrar Terminal"
      Height          =   435
      Left            =   3818
      TabIndex        =   8
      Tag             =   "1"
      Top             =   3180
      Width           =   1845
   End
   Begin VB.CommandButton cmdRenovarLAN 
      Caption         =   "Renovar LAN"
      Height          =   360
      Left            =   1965
      TabIndex        =   7
      Tag             =   "1"
      Top             =   525
      Width           =   1830
   End
   Begin VB.CommandButton cmdIP 
      Caption         =   "�Mi IP P�blica?"
      Height          =   360
      Left            =   98
      TabIndex        =   6
      Tag             =   "1"
      Top             =   525
      Width           =   1830
   End
   Begin VB.TextBox txtSalida 
      Height          =   2220
      Left            =   98
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   5
      Tag             =   "4"
      Top             =   915
      Width           =   5565
   End
   Begin VB.Frame FraDatos 
      Caption         =   "C�digo"
      Height          =   4725
      Left            =   98
      TabIndex        =   2
      Top             =   3690
      Width           =   5565
      Begin VB.TextBox txtCodigos 
         Height          =   3810
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   15
         Top             =   330
         Width           =   5265
      End
      Begin VB.PictureBox picFix 
         BorderStyle     =   0  'None
         Height          =   435
         Left            =   75
         ScaleHeight     =   435
         ScaleWidth      =   1485
         TabIndex        =   3
         Top             =   4245
         Width           =   1485
         Begin VB.CommandButton cmdGuardar 
            Caption         =   "&Guardar"
            Height          =   345
            Left            =   0
            TabIndex        =   4
            Top             =   30
            Width           =   1350
         End
      End
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Resetear"
      Enabled         =   0   'False
      Height          =   360
      Left            =   3833
      TabIndex        =   1
      Tag             =   "1"
      Top             =   525
      Width           =   1830
   End
   Begin VB.ComboBox ComReset 
      Enabled         =   0   'False
      Height          =   315
      Left            =   98
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   75
      Width           =   3915
   End
   Begin VB.Label lblTW 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   345
      Left            =   98
      TabIndex        =   9
      Tag             =   "3"
      Top             =   3240
      Width           =   915
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'   Este archivo es parte del programa "reseter", el c�al es pertenece a SVCommunity.org y a Todosv.com
'   Mantenedores principales:
'    *Kikeuntercio
'    *Vladimir

Private Sub cmdGuardar_Click()
    '<EhHeader>
    On Error GoTo cmdGuardar_Click_Err
    '</EhHeader>
    Memoria_IO ComReset.Text
    Call ComReset_Click
    '<EhFooter>
    Exit Sub
cmdGuardar_Click_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmPrincipal.cmdGuardar_Click"
    Resume Next
    '</EhFooter>
End Sub

Private Sub cmdIP_Click()
    '<EhHeader>
    On Error GoTo cmdIP_Click_Err
    '</EhHeader>
    Cambio_IP
    '<EhFooter>
    Exit Sub
cmdIP_Click_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmPrincipal.cmdIP_Click"
    Resume Next
    '</EhFooter>
End Sub

Private Sub cmdPredefinir_Click()
    '<EhHeader>
    On Error GoTo cmdPredefinir_Click_Err

    '</EhHeader>
    If cmdPredefinir.Caption = "Cancelar" Then
        NetError = True
        cmdPredefinir.Caption = "Predefinir"
        Predefinir False
    Else
        Predefinir True
    End If

    '<EhFooter>
    Exit Sub
cmdPredefinir_Click_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmPrincipal.cmdPredefinir_Click"
    Resume Next
    '</EhFooter>
End Sub

Private Sub cmdReset_Click()
    '<EhHeader>
    On Error GoTo cmdReset_Click_Err
    '</EhHeader>
    Static pestado As Boolean

    If pestado = False Then
        pestado = True
        Registrar "|--Intentando reseteo de " & ComReset.Text
        cmdReset.Caption = "Cancelar"
        Procesar_Codigo
        Registrar "|--Fin de intento de reseteo para " & ComReset.Text
    Else
        NetError = True
    End If

    pestado = False
    cmdReset.Caption = "Resetear"
    '<EhFooter>
    Exit Sub
cmdReset_Click_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmPrincipal.cmdReset_Click"
    Resume Next
    '</EhFooter>
End Sub

Private Sub cmdRenovarLAN_Click()
    'Idea original de Kikeuntercio
    '<EhHeader>
    On Error GoTo cmdRenovarLAN_Click_Err
    '</EhHeader>
    RenovarLAN
    '<EhFooter>
    Exit Sub
cmdRenovarLAN_Click_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmPrincipal.cmdRenovarLAN_Click"
    Resume Next
    '</EhFooter>
End Sub

Private Sub ComReset_Click()
    'Tratamos de recuperar los datos de los routers
    '<EhHeader>
    On Error GoTo ComReset_Click_Err

    '</EhHeader>
    If Existe_Seccion(ComReset.Text) Then 'Si el router existe
        Restablecer_Todo
        IO_Memoria ComReset.Text    'Cargar la configuraci�n
        Mostrar_Datos               'Mostrar la configuraci�n
        cmdPredefinir.Enabled = True
        cmdReset.Enabled = True
    Else
        cmdPredefinir.Enabled = False
        cmdReset.Enabled = False
    End If

    '<EhFooter>
    Exit Sub
ComReset_Click_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmPrincipal.ComReset_Click"
    Resume Next
    '</EhFooter>
End Sub

Private Sub ComReset_Change()
    '<EhHeader>
    On Error GoTo ComReset_Change_Err
    '</EhHeader>
    Call ComReset_Click
    '<EhFooter>
    Exit Sub
ComReset_Change_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmPrincipal.ComReset_Change"
    Resume Next
    '</EhFooter>
End Sub

Private Sub chkAvanzado_Click()
    '<EhHeader>
    On Error GoTo chkAvanzado_Click_Err
    '</EhHeader>
    Height = IIf(chkAvanzado.Value = Checked, 9735, 4050)
    EscribirINI "Interfaz", "MAvanzado", chkAvanzado.Value
    '<EhFooter>
    Exit Sub
chkAvanzado_Click_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmPrincipal.chkAvanzado_Click"
    Resume Next
    '</EhFooter>
End Sub

Private Sub chkTerminal_Click()
    '<EhHeader>
    On Error GoTo chkTerminal_Click_Err

    '</EhHeader>
    If chkTerminal.Value = Checked Then frmTelnet.Show Else frmTelnet.Hide
    EscribirINI "Interfaz", "MTerminal", chkTerminal.Value
    '<EhFooter>
    Exit Sub
chkTerminal_Click_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmPrincipal.chkTerminal_Click"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Form_Load()
    '<EhHeader>
    On Error GoTo Form_Load_Err
    '</EhHeader>
    Colorear Me
    SetAppIcon Me
    '<EhFooter>
    Exit Sub

Form_Load_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmPrincipal.Form_Load"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Form_Resize()
    '<EhHeader>
    On Error Resume Next
    '</EhHeader>
    Move 0, Screen.Height / 2 - Height / 2
    frmTelnet.Move frmPrincipal.Left + frmPrincipal.Width, frmPrincipal.Top - 5, Screen.Width - frmPrincipal.Width
End Sub

Private Sub Form_Unload(Cancel As Integer)
    '<EhHeader>
    On Error GoTo Form_Unload_Err
    '</EhHeader>
    Terminar
    '<EhFooter>
    Exit Sub

Form_Unload_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmPrincipal.Form_Unload"
    Resume Next
    '</EhFooter>
End Sub

Private Sub optTipo_Click(Index As Integer)
    '<EhHeader>
    On Error GoTo optTipo_Click_Err

    '</EhHeader>
    Select Case Index

        Case 0
            m_Datos.tipoAcceso = web

        Case 1
            m_Datos.tipoAcceso = telnet

        Case 2
            m_Datos.tipoAcceso = auro
    End Select

    '<EhFooter>
    Exit Sub
optTipo_Click_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmPrincipal.optTipo_Click"
    Resume Next
    '</EhFooter>
End Sub

Private Sub txtCodigos_Change()
    '<EhHeader>
    On Error GoTo txtCodigos_Change_Err
    '</EhHeader>
    m_Datos.codigo = Replace$(txtCodigos.Text, vbNewLine, Chr(254))
    '<EhFooter>
    Exit Sub
txtCodigos_Change_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmPrincipal.txtCodigos_Change"
    Resume Next
    '</EhFooter>
End Sub
