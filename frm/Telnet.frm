VERSION 5.00
Begin VB.Form frmTelnet 
   BackColor       =   &H80000017&
   BorderStyle     =   0  'None
   ClientHeight    =   5970
   ClientLeft      =   -270
   ClientTop       =   2385
   ClientWidth     =   8220
   FillColor       =   &H00800000&
   BeginProperty Font 
      Name            =   "Fixedsys"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H0000FFFF&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5970
   ScaleWidth      =   8220
   ShowInTaskbar   =   0   'False
   Begin VB.Timer cursor_timer 
      Enabled         =   0   'False
      Interval        =   300
      Left            =   6600
      Top             =   600
   End
End
Attribute VB_Name = "frmTelnet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const GO_NORM = 0
Const GO_IAC1 = 6
Const GO_IAC2 = 7
Const GO_IAC3 = 8
Const GO_IAC4 = 9
Const GO_IAC5 = 10
Const GO_IAC6 = 11
Const SE = 240         'End of Subnegotiation
Const SB = 250         'What follows is subnegotiation
Const WILLTEL = 251
Const WONTTEL = 252
Const DOTEL = 253
Const DONTTEL = 254
Const IAC = 255
Const BINARY = 0
Const ECHO = 1
Const SGA = 3
Const STATUS = 5
Const TIMING = 6
Const TERMTYPE = 24
Const NAWS = 31
Const TERMSPEED = 32
Const TFLOWCNTRL = 33
Const LINEMODE = 34
Const DISPLOC = 35
Const ENVIRON = 36
Const AUTHENTICATION = 37
Const UNKNOWN39 = 39
Public Receiving        As Boolean
Private parsedata(10)   As Integer
Private ppno            As Integer
Private control_on      As Boolean
Private sw_ugoahead As Boolean
Private sw_igoahead As Boolean
Private sw_echo     As Boolean
Private sw_termsent As Boolean
'------------------------------------------------------------
Public Telnet_Connectado As Boolean
Public WithEvents Socket As CSocketMaster
Attribute Socket.VB_VarHelpID = -1

Private Sub cursor_timer_Timer()
    '<EhHeader>
    On Error Resume Next

    '</EhHeader>
    If Not Receiving Then term_DriveCursor
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, _
                         Shift As Integer)
    '<EhHeader>
    On Error GoTo Form_KeyDown_Err
    '</EhHeader>
    Dim CH As String
    CH = Chr$(0)

    'Translate keycodes to VT100 escape sequences
    Select Case KeyCode

        Case vbKeyControl
            control_on = True

        Case vbKeyEnd
            CH = Chr$(27) + "[K"

        Case vbKeyHome
            CH = Chr$(27) + "[H"

        Case vbKeyLeft
            CH = Chr$(27) + "[D"

        Case vbKeyUp
            CH = Chr$(27) + "[A"

        Case vbKeyRight
            CH = Chr$(27) + "[C"

        Case vbKeyDown
            CH = Chr$(27) + "[B"

        Case vbKeyF1
            CH = Chr$(27) + "OP"

        Case vbKeyF2
            CH = Chr$(27) + "OQ"

        Case vbKeyF3
            CH = Chr$(27) + "OR"

        Case vbKeyF4
            CH = Chr$(27) + "OS"

        Case Else

            If control_on And KeyCode > 63 Then
                CH = Chr$(KeyCode - 64)
            End If

    End Select

    If CH > Chr$(0) And Telnet_Connectado Then Socket.SendData CH
    '<EhFooter>
    Exit Sub

Form_KeyDown_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.Form_KeyDown"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    '<EhHeader>
    On Error GoTo Form_KeyPress_Err
    '</EhHeader>
    Dim CH As String

    If Telnet_Connectado Then
        CH = Chr$(KeyAscii)

        If control_on Then
            If KeyAscii > 63 Then
                CH = Chr$(KeyAscii - 64)
            Else
                CH = Chr$(0)
            End If
        End If

        If CH > Chr$(0) Then
            If CH = Chr$(13) Then
                CH = CH & Chr$(10)
            End If

            Socket.SendData CH
        End If
    End If

    '<EhFooter>
    Exit Sub

Form_KeyPress_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.Form_KeyPress"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, _
                       Shift As Integer)
    '<EhHeader>
    On Error GoTo Form_KeyUp_Err

    '</EhHeader>
    Select Case KeyCode

        Case vbKeyControl
            control_on = False
    End Select

    '<EhFooter>
    Exit Sub

Form_KeyUp_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.Form_KeyUp"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Form_Load()
    '<EhHeader>
    On Error GoTo Form_Load_Err
    '</EhHeader>
    Set Socket = New CSocketMaster
    term_init
    '<EhFooter>
    Exit Sub

Form_Load_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.Form_Load"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Form_Paint()
    '<EhHeader>
    On Error GoTo Form_Paint_Err
    '</EhHeader>
    term_redrawscreen
    '<EhFooter>
    Exit Sub

Form_Paint_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.Form_Paint"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, _
                             UnloadMode As Integer)
    '<EhHeader>
    On Error GoTo Form_QueryUnload_Err

    '</EhHeader>
    With Socket
        .CloseSck                            ' Clear any errors...
        .RemoteHost = "0.0.0.0"
        .RemotePort = 0
    End With

    Telnet_Connectado = False
    '<EhFooter>
    Exit Sub

Form_QueryUnload_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.Form_QueryUnload"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Socket_CloseSck()
    '<EhHeader>
    On Error GoTo Socket_CloseSck_Err
    '</EhHeader>
    Telnet_Connectado = False
    '<EhFooter>
    Exit Sub
Socket_CloseSck_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.Socket_CloseSck"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Socket_Connect()
    '<EhHeader>
    On Error GoTo Socket_Connect_Err
    '</EhHeader>
    Dim ConnectString As String
    '------------------------------------------------------------
    sw_ugoahead = True
    sw_igoahead = False
    sw_echo = True
    sw_termsent = False
    ConnectString = Chr$(IAC) & Chr$(DOTEL) & Chr$(ECHO) & Chr$(IAC) & Chr$(DOTEL) & Chr$(SGA) & Chr$(IAC) & Chr$(WILLTEL) & Chr$(NAWS) & Chr$(IAC) & Chr$(WILLTEL) & Chr$(TERMTYPE) & Chr$(IAC) & Chr$(WILLTEL) & Chr$(TERMSPEED)
    Socket.SendData ConnectString
    Telnet_Connectado = True
    '<EhFooter>
    Exit Sub
Socket_Connect_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.Socket_Connect"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Socket_DataArrival(ByVal bytesTotal As Long)
    '<EhHeader>
    On Error GoTo Socket_DataArrival_Err
    '</EhHeader>
    Dim CH()     As Byte
    Dim i        As Integer
    Static cmd   As Byte

    '------------------------------------------------------------
    If Receiving Then
        Exit Sub
    Else
        Receiving = True
        term_CaretControl True
    End If

    If (bytesTotal > 0) Then  ' If there is any data...
        Socket.GetData CH, vbByte + vbArray, bytesTotal
        bytesTotal = bytesTotal - 1

        ' CH = Buf
        For i = 0 To bytesTotal

            Select Case cmd

                Case GO_NORM
                    cmd = term_process_char(CH(i))

                Case GO_IAC1
                    cmd = iac1(CH(i))

                Case GO_IAC2
                    cmd = iac2(CH(i))

                Case GO_IAC3
                    cmd = iac3(CH(i))

                Case GO_IAC4
                    cmd = iac4(CH(i))

                Case GO_IAC5
                    cmd = iac5(CH(i))

                Case GO_IAC6
                    cmd = iac6(CH(i))
            End Select

        Next

    End If

    'Enviamos el siguiente comando
    If nComando < UBound(TelnetComandos) And Socket.State = sckConnected Then
        Socket.SendData TelnetComandos(nComando) & vbCrLf
        Registrar "Telnet -> Enviado: " & TelnetComandos(nComando)
        nComando = nComando + 1
    End If

    term_CaretControl False
    Receiving = False
    '<EhFooter>
    Exit Sub
Socket_DataArrival_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.Socket_DataArrival"
    Resume Next
    '</EhFooter>
End Sub

Private Function iac1(CH As Byte) As Integer
    ' Debug.Print "IAC : ";
    '<EhHeader>
    On Error GoTo iac1_Err
    '</EhHeader>
    iac1 = GO_NORM

    Select Case CH

        Case DOTEL
            iac1 = GO_IAC2

        Case DONTTEL
            iac1 = GO_IAC6

        Case WILLTEL
            iac1 = GO_IAC3

        Case WONTTEL
            iac1 = GO_IAC4

        Case SB
            iac1 = GO_IAC5
            ppno = 0

        Case SE

            ' End of negotiation string, string is in parsedata()
            Select Case parsedata(0)

                Case TERMTYPE

                    If parsedata(1) = 1 Then
                        Socket.SendData Chr$(IAC) & Chr$(SB) & Chr$(TERMTYPE) & "DEC-VT100" & Chr$(0) & Chr$(IAC) & Chr$(SE)
                    End If

                Case TERMSPEED

                    If parsedata(1) = 1 Then
                        ' Debug.Print "TERMSPEED"
                        Socket.SendData Chr$(IAC) & Chr$(WILLTEL) & Chr$(CH)
                        Socket.SendData Chr$(IAC) & Chr$(SB) & Chr$(TERMSPEED) & Chr$(0) & "57600,57600" & Chr$(IAC) & Chr$(SE)
                    End If

            End Select
    End Select

    '<EhFooter>
    Exit Function
iac1_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.iac1"
    Resume Next
    '</EhFooter>
End Function

Private Function iac2(CH As Byte) As Integer
    'DO Processing Respond with WILL or WONT
    '<EhHeader>
    On Error GoTo iac2_Err

    '</EhHeader>
    With Socket
        iac2 = GO_NORM

        Select Case CH

            Case BINARY
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(BINARY)

            Case ECHO
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(ECHO)

            Case NAWS
                .SendData Chr$(IAC) & Chr$(SB) & Chr$(NAWS) & Chr$(0) & Chr$(80) & Chr$(0) & Chr$(24) & Chr$(IAC) & Chr$(SE)

            Case SGA

                If Not sw_igoahead Then
                    .SendData Chr$(IAC) & Chr$(WILLTEL) & Chr$(SGA)
                    sw_igoahead = True
                End If

            Case TERMTYPE

                If Not sw_termsent Then
                    sw_termsent = True
                    .SendData Chr$(IAC) & Chr$(WILLTEL) & Chr$(TERMTYPE)
                    .SendData Chr$(IAC) & Chr$(SB) & Chr$(TERMTYPE) & Chr$(0) & "VT100" & Chr$(IAC) & Chr$(SE)
                End If

            Case TERMSPEED
                .SendData Chr$(IAC) & Chr$(WILLTEL) & Chr$(TERMSPEED)
                .SendData Chr$(IAC) & Chr$(SB) & Chr$(TERMSPEED) & Chr$(0)
                .SendData "57600,57600"
                .SendData Chr$(IAC) & Chr$(SE)

            Case TFLOWCNTRL
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case LINEMODE
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case STATUS
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case TIMING
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case DISPLOC
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case ENVIRON
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case UNKNOWN39
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case AUTHENTICATION
                .SendData Chr$(IAC) & Chr$(WILLTEL) & Chr$(CH)
                .SendData Chr$(IAC) & Chr$(SB) & Chr$(AUTHENTICATION) & Chr$(0) & Chr$(0) & Chr$(0) & Chr$(0) & Chr$(IAC) & Chr$(SE)

            Case Else
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)
        End Select

    End With

    '<EhFooter>
    Exit Function
iac2_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.iac2"
    Resume Next
    '</EhFooter>
End Function

Private Function iac3(CH As Byte) As Integer
    ' WILL Processing - Respond with DO or DONT
    '<EhHeader>
    On Error GoTo iac3_Err

    '</EhHeader>
    With Socket
        iac3 = GO_NORM

        Select Case CH

            Case ECHO

                If Not sw_echo Then
                    sw_echo = True
                    .SendData Chr$(IAC) & Chr$(DOTEL) & Chr$(ECHO)
                End If

            Case SGA

                If Not sw_ugoahead Then
                    sw_ugoahead = True
                    .SendData Chr$(IAC) & Chr$(DOTEL) & Chr$(SGA)
                End If

            Case TERMSPEED
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case TFLOWCNTRL
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case LINEMODE
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case STATUS
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case TIMING
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case DISPLOC
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case ENVIRON
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case UNKNOWN39
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case Else
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)
        End Select

    End With

    '<EhFooter>
    Exit Function
iac3_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.iac3"
    Resume Next
    '</EhFooter>
End Function

Private Function iac4(CH As Byte) As Integer
    ' WONT Processing
    '<EhHeader>
    On Error GoTo iac4_Err

    '</EhHeader>
    With Socket
        iac4 = GO_NORM

        Select Case CH

            Case ECHO

                If sw_echo = True Then
                    .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(ECHO)
                    sw_echo = False
                End If

            Case SGA
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(SGA)
                sw_igoahead = False

            Case TERMSPEED
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case TFLOWCNTRL
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case LINEMODE
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case STATUS
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case TIMING
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case DISPLOC
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case ENVIRON
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case UNKNOWN39
                .SendData Chr$(IAC) & Chr$(DONTTEL) & Chr$(CH)

            Case Else
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)
        End Select

    End With

    '<EhFooter>
    Exit Function
iac4_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.iac4"
    Resume Next
    '</EhFooter>
End Function

Private Function iac5(CH As Byte) As Integer
    '<EhHeader>
    On Error GoTo iac5_Err
    '</EhHeader>
    Dim ich As Integer
    ' Collect parms after SB and until another IAC
    ich = CH

    If ich = IAC Then
        iac5 = GO_IAC1
        Exit Function
    End If

    parsedata(ppno) = ich
    ppno = ppno + 1
    iac5 = GO_IAC5
    '<EhFooter>
    Exit Function
iac5_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.iac5"
    Resume Next
    '</EhFooter>
End Function

Private Function iac6(CH As Byte) As Integer
    'DONT Processing
    '<EhHeader>
    On Error GoTo iac6_Err

    '</EhHeader>
    With Socket
        iac6 = GO_NORM

        Select Case CH

            Case SE

                '
            Case ECHO

                If Not sw_echo Then
                    sw_echo = True
                    .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(ECHO)
                End If

            Case SGA

                If Not sw_ugoahead Then
                    sw_ugoahead = True
                    .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(SGA)
                End If

            Case TERMSPEED
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case TFLOWCNTRL
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case LINEMODE
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case STATUS
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case TIMING
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case DISPLOC
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case ENVIRON
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case UNKNOWN39
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)

            Case Else
                .SendData Chr$(IAC) & Chr$(WONTTEL) & Chr$(CH)
        End Select

    End With

    '<EhFooter>
    Exit Function
iac6_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.iac6"
    Resume Next
    '</EhFooter>
End Function

Private Sub SOCKET_ERROR(ByVal Number As Integer, _
                         Description As String, _
                         ByVal sCode As Long, _
                         ByVal Source As String, _
                         ByVal HelpFile As String, _
                         ByVal HelpContext As Long, _
                         CancelDisplay As Boolean)
    '<EhHeader>
    On Error GoTo SOCKET_ERROR_Err

    '</EhHeader>
    If Number <> 10053 Then
        NetError = True
        Registrar "Telnet -> (" & Number & ") " & Description
    Else
        Registrar "Telnet -> Bien, el router abandon� la conexi�n"
    End If

    '<EhFooter>
    Exit Sub
SOCKET_ERROR_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.SOCKET_ERROR"
    Resume Next
    '</EhFooter>
End Sub

Public Sub ProcTelnet()
    '<EhHeader>
    On Error GoTo ProcTelnet_Err
    '</EhHeader>
    Registrar "~ProcTelnet - Nivel 1"

    With Socket
        .CloseSck
        .RemotePort = m_Datos.puerto
        .RemoteHost = m_Datos.base
        .Protocol = sckTCPProtocol
        .Connect
        term_init
        Registrar "~ProcTelnet - Nivel 2"

        Do Until Telnet_Connectado Or NetError
            Esperar 0.5
        Loop

        Registrar "~ProcTelnet - Nivel 3"
    End With

    '<EhFooter>
    Exit Sub
ProcTelnet_Err:
    Controlar_Error Erl, Err.Description, "Reseter.frmTelnet.ProcTelnet"
    Resume Next
    '</EhFooter>
End Sub
