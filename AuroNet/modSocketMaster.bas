Attribute VB_Name = "modSocketMaster"
'**************************************************************************************
'
'modSocketMaster module 1.2
'Copyright (c) 2004 by Emiliano Scavuzzo <anshoku@yahoo.com>
'
'Rosario, Argentina
'
'**************************************************************************************
'This module contains API declarations and helper functions for the CSocketMaster class
'**************************************************************************************
Option Explicit
'==============================================================================
'API FUNCTIONS
'==============================================================================
'Public Declare Function api_WSAGetLastError Lib "ws2_32.dll" Alias "WSAGetLastError" () As Long
Public Declare Sub api_CopyMemory _
               Lib "kernel32" _
               Alias "RtlMoveMemory" (Destination As Any, _
                                      Source As Any, _
                                      ByVal Length As Long)
Public Declare Function api_GlobalAlloc _
               Lib "kernel32" _
               Alias "GlobalAlloc" (ByVal wFlags As Long, _
                                    ByVal dwBytes As Long) As Long
Public Declare Function api_GlobalFree _
               Lib "kernel32" _
               Alias "GlobalFree" (ByVal hMem As Long) As Long
Private Declare Function api_WSAStartup _
                Lib "ws2_32.dll" _
                Alias "WSAStartup" (ByVal wVersionRequired As Long, _
                                    lpWSADATA As WSAData) As Long
Private Declare Function api_WSACleanup _
                Lib "ws2_32.dll" _
                Alias "WSACleanup" () As Long
Private Declare Function api_WSAAsyncGetHostByName _
                Lib "ws2_32.dll" _
                Alias "WSAAsyncGetHostByName" (ByVal hWnd As Long, _
                                               ByVal wMsg As Long, _
                                               ByVal strHostName As String, _
                                               buf As Any, _
                                               ByVal buflen As Long) As Long
Private Declare Function api_WSAAsyncSelect _
                Lib "ws2_32.dll" _
                Alias "WSAAsyncSelect" (ByVal s As Long, _
                                        ByVal hWnd As Long, _
                                        ByVal wMsg As Long, _
                                        ByVal lEvent As Long) As Long
Private Declare Function api_CreateWindowEx _
                Lib "user32" _
                Alias "CreateWindowExA" (ByVal dwExStyle As Long, _
                                         ByVal lpClassName As String, _
                                         ByVal lpWindowName As String, _
                                         ByVal dwStyle As Long, _
                                         ByVal x As Long, _
                                         ByVal y As Long, _
                                         ByVal nWidth As Long, _
                                         ByVal nHeight As Long, _
                                         ByVal hWndParent As Long, ByVal hMenu As Long, ByVal hInstance As Long, lpParam As Any) As Long
Private Declare Function api_DestroyWindow _
                Lib "user32" _
                Alias "DestroyWindow" (ByVal hWnd As Long) As Long
Private Declare Function api_lstrlen _
                Lib "kernel32" _
                Alias "lstrlenA" (ByVal lpString As Any) As Long
Private Declare Function api_lstrcpy _
                Lib "kernel32" _
                Alias "lstrcpyA" (ByVal lpString1 As String, _
                                  ByVal lpString2 As Long) As Long
'==============================================================================
'CONSTANTS
'==============================================================================
Public Const SOCKET_ERROR   As Integer = -1
Public Const INVALID_SOCKET As Integer = -1
Public Const INADDR_NONE As Long = &HFFFF
Private Const WSADESCRIPTION_LEN As Integer = 257
Private Const WSASYS_STATUS_LEN  As Integer = 129
Private Enum WinsockVersion
    SOCKET_VERSION_11 = &H101
    SOCKET_VERSION_22 = &H202
End Enum
Public Const MAXGETHOSTSTRUCT As Long = 1024
Public Const AF_INET        As Long = 2
Public Const SOCK_STREAM    As Long = 1
Public Const SOCK_DGRAM     As Long = 2
Public Const IPPROTO_TCP    As Long = 6
Public Const IPPROTO_UDP    As Long = 17
Public Const FD_READ    As Integer = &H1&
Public Const FD_WRITE   As Integer = &H2&
Public Const FD_ACCEPT  As Integer = &H8&
Public Const FD_CONNECT As Integer = &H10&
Public Const FD_CLOSE   As Integer = &H20&
Private Const OFFSET_2 As Long = 65536
Private Const MAXINT_2 As Long = 32767
Public Const GMEM_FIXED As Integer = &H0
Public Const LOCAL_HOST_BUFF As Integer = 256
Public Const SOL_SOCKET         As Long = 65535
Public Const SO_SNDBUF          As Long = &H1001&
Public Const SO_RCVBUF          As Long = &H1002&
Public Const SO_MAX_MSG_SIZE    As Long = &H2003
Public Const SO_BROADCAST       As Long = &H20
Public Const FIONREAD           As Long = &H4004667F
'==============================================================================
'ERROR CODES
'==============================================================================
Public Const WSABASEERR         As Long = 10000
Public Const WSAEINTR           As Long = (WSABASEERR + 4)
Public Const WSAEACCES          As Long = (WSABASEERR + 13)
Public Const WSAEFAULT          As Long = (WSABASEERR + 14)
Public Const WSAEINVAL          As Long = (WSABASEERR + 22)
Public Const WSAEMFILE          As Long = (WSABASEERR + 24)
Public Const WSAEWOULDBLOCK     As Long = (WSABASEERR + 35)
Public Const WSAEINPROGRESS     As Long = (WSABASEERR + 36)
Public Const WSAEALREADY        As Long = (WSABASEERR + 37)
Public Const WSAENOTSOCK        As Long = (WSABASEERR + 38)
Public Const WSAEDESTADDRREQ    As Long = (WSABASEERR + 39)
Public Const WSAEMSGSIZE        As Long = (WSABASEERR + 40)
Public Const WSAEPROTOTYPE      As Long = (WSABASEERR + 41)
Public Const WSAENOPROTOOPT     As Long = (WSABASEERR + 42)
Public Const WSAEPROTONOSUPPORT As Long = (WSABASEERR + 43)
Public Const WSAESOCKTNOSUPPORT As Long = (WSABASEERR + 44)
Public Const WSAEOPNOTSUPP      As Long = (WSABASEERR + 45)
Public Const WSAEPFNOSUPPORT    As Long = (WSABASEERR + 46)
Public Const WSAEAFNOSUPPORT    As Long = (WSABASEERR + 47)
Public Const WSAEADDRINUSE      As Long = (WSABASEERR + 48)
Public Const WSAEADDRNOTAVAIL   As Long = (WSABASEERR + 49)
Public Const WSAENETDOWN        As Long = (WSABASEERR + 50)
Public Const WSAENETUNREACH     As Long = (WSABASEERR + 51)
Public Const WSAENETRESET       As Long = (WSABASEERR + 52)
Public Const WSAECONNABORTED    As Long = (WSABASEERR + 53)
Public Const WSAECONNRESET      As Long = (WSABASEERR + 54)
Public Const WSAENOBUFS         As Long = (WSABASEERR + 55)
Public Const WSAEISCONN         As Long = (WSABASEERR + 56)
Public Const WSAENOTCONN        As Long = (WSABASEERR + 57)
Public Const WSAESHUTDOWN       As Long = (WSABASEERR + 58)
Public Const WSAETIMEDOUT       As Long = (WSABASEERR + 60)
Public Const WSAEHOSTUNREACH    As Long = (WSABASEERR + 65)
Public Const WSAECONNREFUSED    As Long = (WSABASEERR + 61)
Public Const WSAEPROCLIM        As Long = (WSABASEERR + 67)
Public Const WSASYSNOTREADY     As Long = (WSABASEERR + 91)
Public Const WSAVERNOTSUPPORTED As Long = (WSABASEERR + 92)
Public Const WSANOTINITIALISED  As Long = (WSABASEERR + 93)
Public Const WSAHOST_NOT_FOUND  As Long = (WSABASEERR + 1001)
Public Const WSATRY_AGAIN       As Long = (WSABASEERR + 1002)
Public Const WSANO_RECOVERY     As Long = (WSABASEERR + 1003)
Public Const WSANO_DATA         As Long = (WSABASEERR + 1004)
'==============================================================================
'WINSOCK CONTROL ERROR CODES
'==============================================================================
Public Const sckOutOfMemory As Long = 7
Public Const sckBadState    As Long = 40006
Public Const sckInvalidArg  As Long = 40014
Public Const sckUnsupported As Long = 40018
Public Const sckInvalidOp   As Long = 40020
'==============================================================================
'STRUCTURES
'==============================================================================
Private Type WSAData
    wVersion       As Integer
    wHighVersion   As Integer
    szDescription  As String * WSADESCRIPTION_LEN
    szSystemStatus As String * WSASYS_STATUS_LEN
    iMaxSockets    As Integer
    iMaxUdpDg      As Integer
    lpVendorInfo   As Long
End Type
Public Type HOSTENT
    hName     As Long
    hAliases  As Long
    hAddrType As Integer
    hLength   As Integer
    hAddrList As Long
End Type
Public Type sockaddr_in
    sin_family       As Integer
    sin_port         As Integer
    sin_addr         As Long
    sin_zero(1 To 8) As Byte
End Type
'==============================================================================
'MEMBER VARIABLES
'==============================================================================
Private m_blnInitiated          As Boolean      'specify if winsock service was initiated
Private m_lngSocksQuantity      As Long         'number of instances created
Private m_colSocketsInst        As Collection   'sockets list and instance owner
Private m_colAcceptList         As Collection   'sockets in queue that need to be accepted
Private m_lngWindowHandle       As Long         'message window handle
'==============================================================================
'SUBCLASSING DECLARATIONS
'by Paul Caton
'==============================================================================
Private Declare Function api_IsWindow _
                Lib "user32" _
                Alias "IsWindow" (ByVal hWnd As Long) As Long
Private Declare Function api_GetWindowLong _
                Lib "user32" _
                Alias "GetWindowLongA" (ByVal hWnd As Long, _
                                        ByVal nIndex As Long) As Long
Private Declare Function api_SetWindowLong _
                Lib "user32" _
                Alias "SetWindowLongA" (ByVal hWnd As Long, _
                                        ByVal nIndex As Long, _
                                        ByVal dwNewLong As Long) As Long
Private Declare Function api_GetModuleHandle _
                Lib "kernel32" _
                Alias "GetModuleHandleA" (ByVal lpModuleName As String) As Long
Private Declare Function api_GetProcAddress _
                Lib "kernel32" _
                Alias "GetProcAddress" (ByVal hModule As Long, _
                                        ByVal lpProcName As String) As Long
Private Const PATCH_06 As Long = 106
Private Const PATCH_09 As Long = 137
Private Const GWL_WNDPROC As Long = (-4)
Private Const WM_APP As Long = 32768 '0x8000
Public Const RESOLVE_MESSAGE As Long = WM_APP
Public Const SOCKET_MESSAGE  As Long = WM_APP + 1
Private lngMsgCntA      As Long     'TableA entry count
Private lngMsgCntB      As Long     'TableB entry count
Private lngTableA1()    As Long     'TableA1: list of async handles
Private lngTableA2()    As Long     'TableA2: list of async handles owners
Private lngTableB1()    As Long     'TableB1: list of sockets
Private lngTableB2()    As Long     'TableB2: list of sockets owners
Private hWndSub         As Long     'window handle subclassed
Private nAddrSubclass   As Long     'address of our WndProc
Private nAddrOriginal   As Long     'address of original WndProc

'Once we are done with the class instance we call this
'function to discount it and finish winsock service if
'it was the last one.
'Returns 0 if it has success.
Public Function FinalizeProcesses() As Long
    '<EhHeader>
    On Error GoTo FinalizeProcesses_Err
    '</EhHeader>
    FinalizeProcesses = 0
    m_lngSocksQuantity = m_lngSocksQuantity - 1

    'if the service was initiated and there's no more instances
    'of the class then we finish the service
    If m_blnInitiated And m_lngSocksQuantity = 0 Then
        If FinalizeService = SOCKET_ERROR Then
            Dim lngErrorCode As Long
            lngErrorCode = Err.LastDllError
            FinalizeProcesses = lngErrorCode
            Err.Raise lngErrorCode, "modSocketMaster.FinalizeProcesses", GetErrorDescription(lngErrorCode)
        Else
            '114             Debug.Print "OK Winsock service finalized"
        End If

        Subclass_Terminate
        m_blnInitiated = False
    End If

    '<EhFooter>
    Exit Function
FinalizeProcesses_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.FinalizeProcesses"
    Resume Next
    '</EhFooter>
End Function

'Return the accept instance class from a socket.
Public Function GetAcceptClass(ByVal lngSocket As Long) As CSocketMaster
    '<EhHeader>
    On Error GoTo GetAcceptClass_Err
    '</EhHeader>
    Set GetAcceptClass = m_colAcceptList("S" & lngSocket)
    '<EhFooter>
    Exit Function
GetAcceptClass_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.GetAcceptClass"
    Resume Next
    '</EhFooter>
End Function

'This function receives a number that represents an error
'and returns the corresponding description string.
Public Function GetErrorDescription(ByVal lngErrorCode As Long) As String
    '<EhHeader>
    On Error GoTo GetErrorDescription_Err

    '</EhHeader>
    Select Case lngErrorCode

        Case WSAEACCES
            GetErrorDescription = "Permission denied."

        Case WSAEADDRINUSE
            GetErrorDescription = "Address already in use."

        Case WSAEADDRNOTAVAIL
            GetErrorDescription = "Cannot assign requested address."

        Case WSAEAFNOSUPPORT
            GetErrorDescription = "Address family not supported by protocol family."

        Case WSAEALREADY
            GetErrorDescription = "Operation already in progress."

        Case WSAECONNABORTED
            GetErrorDescription = "Software caused connection abort."

        Case WSAECONNREFUSED
            GetErrorDescription = "Connection refused."

        Case WSAECONNRESET
            GetErrorDescription = "Connection reset by peer."

        Case WSAEDESTADDRREQ
            GetErrorDescription = "Destination address required."

        Case WSAEFAULT
            GetErrorDescription = "Bad address."

        Case WSAEHOSTUNREACH
            GetErrorDescription = "No route to host."

        Case WSAEINPROGRESS
            GetErrorDescription = "Operation now in progress."

        Case WSAEINTR
            GetErrorDescription = "Interrupted function call."

        Case WSAEINVAL
            GetErrorDescription = "Invalid argument."

        Case WSAEISCONN
            GetErrorDescription = "Socket is already connected."

        Case WSAEMFILE
            GetErrorDescription = "Too many open files."

        Case WSAEMSGSIZE
            GetErrorDescription = "Message too long."

        Case WSAENETDOWN
            GetErrorDescription = "Network is down."

        Case WSAENETRESET
            GetErrorDescription = "Network dropped connection on reset."

        Case WSAENETUNREACH
            GetErrorDescription = "Network is unreachable."

        Case WSAENOBUFS
            GetErrorDescription = "No buffer space available."

        Case WSAENOPROTOOPT
            GetErrorDescription = "Bad protocol option."

        Case WSAENOTCONN
            GetErrorDescription = "Socket is not connected."

        Case WSAENOTSOCK
            GetErrorDescription = "Socket operation on nonsocket."

        Case WSAEOPNOTSUPP
            GetErrorDescription = "Operation not supported."

        Case WSAEPFNOSUPPORT
            GetErrorDescription = "Protocol family not supported."

        Case WSAEPROCLIM
            GetErrorDescription = "Too many processes."

        Case WSAEPROTONOSUPPORT
            GetErrorDescription = "Protocol not supported."

        Case WSAEPROTOTYPE
            GetErrorDescription = "Protocol wrong type for socket."

        Case WSAESHUTDOWN
            GetErrorDescription = "Cannot send after socket shutdown."

        Case WSAESOCKTNOSUPPORT
            GetErrorDescription = "Socket type not supported."

        Case WSAETIMEDOUT
            GetErrorDescription = "Connection timed out."

        Case WSAEWOULDBLOCK
            GetErrorDescription = "Resource temporarily unavailable."

        Case WSAHOST_NOT_FOUND
            GetErrorDescription = "Host not found."

        Case WSANOTINITIALISED
            GetErrorDescription = "Successful WSAStartup not yet performed."

        Case WSANO_DATA
            GetErrorDescription = "Valid name, no data record of requested type."

        Case WSANO_RECOVERY
            GetErrorDescription = "This is a nonrecoverable error."

        Case WSASYSNOTREADY
            GetErrorDescription = "Network subsystem is unavailable."

        Case WSATRY_AGAIN
            GetErrorDescription = "Non authoritative host not found."

        Case WSAVERNOTSUPPORTED
            GetErrorDescription = "Winsock.dll version out of range."

        Case Else
            GetErrorDescription = "Unknown error."
    End Select

    '<EhFooter>
    Exit Function
GetErrorDescription_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.GetErrorDescription"
    Resume Next
    '</EhFooter>
End Function

'Returns the hi word from a double word.
Public Function HiWord(lngValue As Long) As Long
    '<EhHeader>
    On Error GoTo HiWord_Err

    '</EhHeader>
    If (lngValue And &H80000000) = &H80000000 Then
        HiWord = ((lngValue And &H7FFF0000) \ &H10000) Or &H8000&
    Else
        HiWord = (lngValue And &HFFFF0000) \ &H10000
    End If

    '<EhFooter>
    Exit Function
HiWord_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.HiWord"
    Resume Next
    '</EhFooter>
End Function

'This function initiates the processes needed to keep
'control of sockets. Returns 0 if it has success.
Public Function InitiateProcesses() As Long
    '<EhHeader>
    On Error GoTo InitiateProcesses_Err
    '</EhHeader>
    InitiateProcesses = 0
    m_lngSocksQuantity = m_lngSocksQuantity + 1

    'if the service wasn't initiated yet we do it now
    If Not m_blnInitiated Then
        Subclass_Initialize
        m_blnInitiated = True
        Dim lngResult As Long
        lngResult = InitiateService

        If lngResult = 0 Then
            'Debug.Print "OK Winsock service initiated"
        Else
            Debug.Print "ERROR trying to initiate winsock service"
            Err.Raise lngResult, "modSocketMaster.InitiateProcesses", GetErrorDescription(lngResult)
            InitiateProcesses = lngResult
        End If
    End If

    '<EhFooter>
    Exit Function
InitiateProcesses_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.InitiateProcesses"
    Resume Next
    '</EhFooter>
End Function

'The function takes a Long containing a value in the range 
'of an unsigned Integer and returns an Integer that you 
'can pass to an API that requires an unsigned Integer
Public Function IntegerToUnsigned(Value As Integer) As Long
    '<EhHeader>
    On Error GoTo IntegerToUnsigned_Err

    '</EhHeader>
    If Value < 0 Then
        IntegerToUnsigned = Value + OFFSET_2
    Else
        IntegerToUnsigned = Value
    End If

    '<EhFooter>
    Exit Function
IntegerToUnsigned_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.IntegerToUnsigned"
    Resume Next
    '</EhFooter>
End Function

'Returns True is lngSocket is registered on the
'accept list.
Public Function IsAcceptRegistered(ByVal lngSocket As Long) As Boolean
    '<EhHeader>
    On Error GoTo IsAcceptRegistered_Err
    '</EhHeader>
    On Error GoTo Error_Handler
    m_colAcceptList.Item ("S" & lngSocket)
    IsAcceptRegistered = True
    Exit Function
Error_Handler:
    IsAcceptRegistered = False
    '<EhFooter>
    Exit Function
IsAcceptRegistered_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.IsAcceptRegistered"
    Resume Next
    '</EhFooter>
End Function

'Returns TRUE si the socket that is passed is registered
'in the colSocketsInst collection.
Public Function IsSocketRegistered(ByVal lngSocket As Long) As Boolean
    '<EhHeader>
    On Error GoTo IsSocketRegistered_Err
    '</EhHeader>
    On Error GoTo Error_Handler
    m_colSocketsInst.Item ("S" & lngSocket)
    IsSocketRegistered = True
    Exit Function
Error_Handler:
    IsSocketRegistered = False
    '<EhFooter>
    Exit Function
IsSocketRegistered_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.IsSocketRegistered"
    Resume Next
    '</EhFooter>
End Function

'Returns the low word from a double word.
Public Function LoWord(lngValue As Long) As Long
    '<EhHeader>
    On Error GoTo LoWord_Err
    '</EhHeader>
    LoWord = (lngValue And &HFFFF&)
    '<EhFooter>
    Exit Function
LoWord_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.LoWord"
    Resume Next
    '</EhFooter>
End Function

'Assing a temporal instance of CSocketMaster to a
'socket and register this socket to the accept list.
Public Sub RegisterAccept(ByVal lngSocket As Long)
    '<EhHeader>
    On Error GoTo RegisterAccept_Err

    '</EhHeader>
    If m_colAcceptList Is Nothing Then
        Set m_colAcceptList = New Collection
    End If

    Dim Socket As CSocketMaster
    Set Socket = New CSocketMaster
    Socket.Accept lngSocket
    m_colAcceptList.Add Socket, "S" & lngSocket
    '<EhFooter>
    Exit Sub
RegisterAccept_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.RegisterAccept"
    Resume Next
    '</EhFooter>
End Sub

'Adds the socket to the m_colSocketsInst collection, and
'registers that socket with WSAAsyncSelect Winsock API
'function to receive network events for the socket.
'If this socket is the first one to be registered, the
'window and collection will be created in this function as well.
Public Function RegisterSocket(ByVal lngSocket As Long, _
                               ByVal lngObjectPointer As Long, _
                               ByVal blnEvents As Boolean) As Boolean
    '<EhHeader>
    On Error GoTo RegisterSocket_Err

    '</EhHeader>
    If m_colSocketsInst Is Nothing Then
        Set m_colSocketsInst = New Collection

        If CreateWinsockMessageWindow <> 0 Then
            Err.Raise sckOutOfMemory, "modSocketMaster.RegisterSocket", "Out of memory"
        End If

        Subclass_Subclass (m_lngWindowHandle)
    End If

    Subclass_AddSocketMessage lngSocket, lngObjectPointer

    'Do we need to register socket events?
    If blnEvents Then
        Dim lngEvents As Long
        Dim lngResult As Long
        Dim lngErrorCode As Long
        lngEvents = FD_READ Or FD_WRITE Or FD_ACCEPT Or FD_CONNECT Or FD_CLOSE
        lngResult = api_WSAAsyncSelect(lngSocket, m_lngWindowHandle, SOCKET_MESSAGE, lngEvents)

        If lngResult = SOCKET_ERROR Then
            Debug.Print "ERROR trying to register events from socket " & lngSocket
            lngErrorCode = Err.LastDllError
            Err.Raise lngErrorCode, "modSocketMaster.RegisterSocket", GetErrorDescription(lngErrorCode)
        End If
    End If

    m_colSocketsInst.Add lngObjectPointer, "S" & lngSocket
    RegisterSocket = True
    '<EhFooter>
    Exit Function
RegisterSocket_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.RegisterSocket"
    Resume Next
    '</EhFooter>
End Function

'When a socket needs to resolve a hostname in asynchronous way
'it calls this function. If it has success it returns a nonzero
'number that represents the async task handle and register this
'number in the TableA list.
'Returns 0 if it fails.
Public Function ResolveHost(ByVal strHost As String, _
                            ByVal lngHOSTENBuf As Long, _
                            ByVal lngObjectPointer As Long) As Long
    '<EhHeader>
    On Error GoTo ResolveHost_Err
    '</EhHeader>
    Dim lngAsynHandle As Long
    lngAsynHandle = api_WSAAsyncGetHostByName(m_lngWindowHandle, RESOLVE_MESSAGE, strHost, ByVal lngHOSTENBuf, MAXGETHOSTSTRUCT)

    If lngAsynHandle <> 0 Then Subclass_AddResolveMessage lngAsynHandle, lngObjectPointer
    ResolveHost = lngAsynHandle
    '<EhFooter>
    Exit Function
ResolveHost_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.ResolveHost"
    Resume Next
    '</EhFooter>
End Function

'Receives a string pointer and it turns it into a regular string.
Public Function StringFromPointer(ByVal lPointer As Long) As String
    '<EhHeader>
    On Error GoTo StringFromPointer_Err
    '</EhHeader>
    Dim strTemp As String
    Dim lRetVal As Long
    strTemp = String$(api_lstrlen(ByVal lPointer), 0)
    lRetVal = api_lstrcpy(ByVal strTemp, ByVal lPointer)

    If lRetVal Then StringFromPointer = strTemp
    '<EhFooter>
    Exit Function
StringFromPointer_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.StringFromPointer"
    Resume Next
    '</EhFooter>
End Function

'Unregister lngSocket from the accept list.
Public Sub UnregisterAccept(ByVal lngSocket As Long)
    '<EhHeader>
    On Error GoTo UnregisterAccept_Err
    '</EhHeader>
    m_colAcceptList.Remove "S" & lngSocket

    If m_colAcceptList.Count = 0 Then
        Set m_colAcceptList = Nothing
    End If

    '<EhFooter>
    Exit Sub
UnregisterAccept_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.UnregisterAccept"
    Resume Next
    '</EhFooter>
End Sub

'When ResolveHost is called an async task handle is added
'to TableA list. Use this function to remove that record.
Public Sub UnregisterResolution(ByVal lngAsynHandle As Long)
    '<EhHeader>
    On Error GoTo UnregisterResolution_Err
    '</EhHeader>
    Subclass_DelResolveMessage lngAsynHandle
    '<EhFooter>
    Exit Sub
UnregisterResolution_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.UnregisterResolution"
    Resume Next
    '</EhFooter>
End Sub

'Removes the socket from the m_colSocketsInst collection
'If it is the last socket in that collection, the window
'and colection will be destroyed as well.
Public Sub UnregisterSocket(ByVal lngSocket As Long)
    '<EhHeader>
    On Error GoTo UnregisterSocket_Err
    '</EhHeader>
    Subclass_DelSocketMessage lngSocket
    On Error Resume Next
    m_colSocketsInst.Remove "S" & lngSocket

    If m_colSocketsInst.Count = 0 Then
        Set m_colSocketsInst = Nothing
        Subclass_UnSubclass
        DestroyWinsockMessageWindow
    End If

    '<EhFooter>
    Exit Sub
UnregisterSocket_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.UnregisterSocket"
    Resume Next
    '</EhFooter>
End Sub

'The function takes an unsigned Integer from and API and 
'converts it to a Long for display or arithmetic purposes
Public Function UnsignedToInteger(Value As Long) As Integer
    '<EhHeader>
    On Error GoTo UnsignedToInteger_Err

    '</EhHeader>
    If Value < 0 Or Value >= OFFSET_2 Then Error 6 ' Overflow
    If Value <= MAXINT_2 Then
        UnsignedToInteger = Value
    Else
        UnsignedToInteger = Value - OFFSET_2
    End If

    '<EhFooter>
    Exit Function
UnsignedToInteger_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.UnsignedToInteger"
    Resume Next
    '</EhFooter>
End Function

'Create a window that is used to capture sockets messages.
'Returns 0 if it has success.
Private Function CreateWinsockMessageWindow() As Long
    '<EhHeader>
    On Error GoTo CreateWinsockMessageWindow_Err
    '</EhHeader>
    m_lngWindowHandle = api_CreateWindowEx(0&, "STATIC", "SOCKET_WINDOW", 0&, 0&, 0&, 0&, 0&, 0&, 0&, App.hInstance, ByVal 0&)

    If m_lngWindowHandle = 0 Then
        CreateWinsockMessageWindow = sckOutOfMemory
        Exit Function
    Else
        CreateWinsockMessageWindow = 0
    End If

    '<EhFooter>
    Exit Function
CreateWinsockMessageWindow_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.CreateWinsockMessageWindow"
    Resume Next
    '</EhFooter>
End Function

'Destroy the window that is used to capture sockets messages.
'Returns 0 if it has success.
Private Function DestroyWinsockMessageWindow() As Long
    '<EhHeader>
    On Error GoTo DestroyWinsockMessageWindow_Err
    '</EhHeader>
    DestroyWinsockMessageWindow = 0

    If m_lngWindowHandle = 0 Then
        Exit Function
    End If

    Dim lngResult As Long
    lngResult = api_DestroyWindow(m_lngWindowHandle)

    If lngResult = 0 Then
        DestroyWinsockMessageWindow = sckOutOfMemory
        Err.Raise sckOutOfMemory, "modSocketMaster.DestroyWinsockMessageWindow", "Out of memory"
    Else
        '112         Debug.Print "OK Destroyed winsock message window " & m_lngWindowHandle
        m_lngWindowHandle = 0
    End If

    '<EhFooter>
    Exit Function
DestroyWinsockMessageWindow_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.DestroyWinsockMessageWindow"
    Resume Next
    '</EhFooter>
End Function

'Finish winsock service calling the function
'api_WSACleanup and returns the result.
Private Function FinalizeService() As Long
    '<EhHeader>
    On Error GoTo FinalizeService_Err
    '</EhHeader>
    Dim lngResultado As Long
    lngResultado = api_WSACleanup
    FinalizeService = lngResultado
    '<EhFooter>
    Exit Function
FinalizeService_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.FinalizeService"
    Resume Next
    '</EhFooter>
End Function

'This function initiate the winsock service calling
'the api_WSAStartup funtion and returns resulting value.
Private Function InitiateService() As Long
    '<EhHeader>
    On Error GoTo InitiateService_Err
    '</EhHeader>
    Dim udtWSAData As WSAData
    Dim lngResult As Long
    lngResult = api_WSAStartup(SOCKET_VERSION_11, udtWSAData)
    InitiateService = lngResult
    '<EhFooter>
    Exit Function
InitiateService_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.InitiateService"
    Resume Next
    '</EhFooter>
End Function

Public Sub Subclass_ChangeOwner(ByVal lngSocket As Long, _
                                ByVal lngObjectPointer As Long)
    '<EhHeader>
    On Error GoTo Subclass_ChangeOwner_Err
    '</EhHeader>
    Dim Count As Long

    For Count = 1 To lngMsgCntB

        If lngTableB1(Count) = lngSocket Then
            lngTableB2(Count) = lngObjectPointer
            Exit Sub
        End If

    Next

    '<EhFooter>
    Exit Sub
Subclass_ChangeOwner_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_ChangeOwner"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Subclass_AddResolveMessage(ByVal lngAsync As Long, _
                                       ByVal lngObjectPointer As Long)
    '<EhHeader>
    On Error GoTo Subclass_AddResolveMessage_Err
    '</EhHeader>
    Dim Count As Long

    For Count = 1 To lngMsgCntA

        Select Case lngTableA1(Count)

            Case -1
                lngTableA1(Count) = lngAsync
                lngTableA2(Count) = lngObjectPointer
                Exit Sub

            Case lngAsync
                Exit Sub
        End Select

    Next

    lngMsgCntA = lngMsgCntA + 1
    ReDim Preserve lngTableA1(1 To lngMsgCntA)
    ReDim Preserve lngTableA2(1 To lngMsgCntA)
    lngTableA1(lngMsgCntA) = lngAsync
    lngTableA2(lngMsgCntA) = lngObjectPointer
    Subclass_PatchTableA
    '<EhFooter>
    Exit Sub
Subclass_AddResolveMessage_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_AddResolveMessage"
    Resume Next
    '</EhFooter>
End Sub

'Return the address of the passed function in the passed dll
Private Function Subclass_AddrFunc(ByVal sDLL As String, _
                                   ByVal sProc As String) As Long
    '<EhHeader>
    On Error GoTo Subclass_AddrFunc_Err
    '</EhHeader>
    Subclass_AddrFunc = api_GetProcAddress(api_GetModuleHandle(sDLL), sProc)
    '<EhFooter>
    Exit Function
Subclass_AddrFunc_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_AddrFunc"
    Resume Next
    '</EhFooter>
End Function

'Return the address of the low bound of the passed table array
Private Function Subclass_AddrMsgTbl(ByRef aMsgTbl() As Long) As Long
    '<EhHeader>
    On Error GoTo Subclass_AddrMsgTbl_Err
    '</EhHeader>
    On Error Resume Next                                    'The table may not be dimensioned yet so we need protection
    Subclass_AddrMsgTbl = VarPtr(aMsgTbl(1))              'Get the address of the first element of the passed message table
    On Error GoTo Subclass_AddrMsgTbl_Err                                         'Switch off error protection
    '<EhFooter>
    Exit Function
Subclass_AddrMsgTbl_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_AddrMsgTbl"
    Resume Next
    '</EhFooter>
End Function

Private Sub Subclass_AddSocketMessage(ByVal lngSocket As Long, _
                                      ByVal lngObjectPointer As Long)
    '<EhHeader>
    On Error GoTo Subclass_AddSocketMessage_Err
    '</EhHeader>
    Dim Count As Long

    For Count = 1 To lngMsgCntB

        Select Case lngTableB1(Count)

            Case -1
                lngTableB1(Count) = lngSocket
                lngTableB2(Count) = lngObjectPointer
                Exit Sub

            Case lngSocket
                Exit Sub
        End Select

    Next

    lngMsgCntB = lngMsgCntB + 1
    ReDim Preserve lngTableB1(1 To lngMsgCntB)
    ReDim Preserve lngTableB2(1 To lngMsgCntB)
    lngTableB1(lngMsgCntB) = lngSocket
    lngTableB2(lngMsgCntB) = lngObjectPointer
    Subclass_PatchTableB
    '<EhFooter>
    Exit Sub
Subclass_AddSocketMessage_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_AddSocketMessage"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Subclass_DelResolveMessage(ByVal lngAsync As Long)
    '<EhHeader>
    On Error GoTo Subclass_DelResolveMessage_Err
    '</EhHeader>
    Dim Count As Long

    For Count = 1 To lngMsgCntA

        If lngTableA1(Count) = lngAsync Then
            lngTableA1(Count) = -1
            lngTableA2(Count) = -1
            Exit Sub
        End If

    Next

    '<EhFooter>
    Exit Sub
Subclass_DelResolveMessage_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_DelResolveMessage"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Subclass_DelSocketMessage(ByVal lngSocket As Long)
    '<EhHeader>
    On Error GoTo Subclass_DelSocketMessage_Err
    '</EhHeader>
    Dim Count As Long

    For Count = 1 To lngMsgCntB

        If lngTableB1(Count) = lngSocket Then
            lngTableB1(Count) = -1
            lngTableB2(Count) = -1
            Exit Sub
        End If

    Next

    '<EhFooter>
    Exit Sub
Subclass_DelSocketMessage_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_DelSocketMessage"
    Resume Next
    '</EhFooter>
End Sub

'Return whether we're running in the IDE. Public for general utility purposes
Private Function Subclass_InIDE() As Boolean
    '<EhHeader>
    On Error GoTo Subclass_InIDE_Err
    '</EhHeader>
    Debug.Assert Subclass_SetTrue(Subclass_InIDE)
    '<EhFooter>
    Exit Function
Subclass_InIDE_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_InIDE"
    Resume Next
    '</EhFooter>
End Function

'==============================================================================
'SUBCLASSING CODE
'based on code by Paul Caton
'==============================================================================
Private Sub Subclass_Initialize()
    '<EhHeader>
    On Error GoTo Subclass_Initialize_Err
    '</EhHeader>
    Const PATCH_01 As Long = 15                                 'Code buffer offset to the location of the relative address to EbMode
    Const PATCH_03 As Long = 76                                 'Relative address of SetWindowsLong
    Const PATCH_05 As Long = 100                                 'Relative address of CallWindowProc
    Const FUNC_EBM As String = "EbMode"                         'VBA's EbMode function allows the machine code thunk to know if the IDE has stopped or is on a breakpoint
    Const FUNC_SWL As String = "SetWindowLongA"                 'SetWindowLong allows the cSubclasser machine code thunk to unsubclass the subclasser itself if it detects via the EbMode function that the IDE has stopped
    Const FUNC_CWP As String = "CallWindowProcA"                'We use CallWindowProc to call the original WndProc
    Const MOD_VBA5 As String = "vba5"                           'Location of the EbMode function if running VB5
    Const MOD_VBA6 As String = "vba6"                           'Location of the EbMode function if running VB6
    Const MOD_USER As String = "user32"                         'Location of the SetWindowLong & CallWindowProc functions
    Dim i        As Long                                      'Loop index
    Dim nLen     As Long                                      'String lengths
    Dim sHex     As String                                    'Hex code string
    Dim sCode    As String                                    'Binary code string
    'Store the hex pair machine code representation in sHex
    sHex = "5850505589E55753515231C0EB0EE8xxxxx01x83F802742285C074258B45103D0080000074433D01800000745BE8200000005A595B5FC9C21400E813000000EBF168xxxxx02x6AFCFF750CE8xxxxx03xEBE0FF7518FF7514FF7510FF750C68xxxxx04xE8xxxxx05xC3BBxxxxx06x8B4514BFxxxxx07x89D9F2AF75B629CB4B8B1C9Dxxxxx08xEB1DBBxxxxx09x8B4514BFxxxxx0Ax89D9F2AF759729CB4B8B1C9Dxxxxx0Bx895D088B1B8B5B1C89D85A595B5FC9FFE0"
    nLen = Len(sHex)                                          'Length of hex pair string

    'Convert the string from hex pairs to bytes and store in the ASCII string opcode buffer
    For i = 1 To nLen Step 2                                  'For each pair of hex characters
        sCode = sCode & ChrB$(Val("&H" & Mid$(sHex, i, 2)))     'Convert a pair of hex characters to a byte and append to the ASCII string
    Next                                                     'Next pair

    nLen = LenB(sCode)                                        'Get the machine code length
    nAddrSubclass = api_GlobalAlloc(0, nLen)                  'Allocate fixed memory for machine code buffer
    'Copy the code to allocated memory
    Call api_CopyMemory(ByVal nAddrSubclass, ByVal StrPtr(sCode), nLen)

    If Subclass_InIDE Then
        'Patch the jmp (EB0E) with two nop's (90) enabling the IDE breakpoint/stop checking code
        Call api_CopyMemory(ByVal nAddrSubclass + 12, &H9090, 2)
        i = Subclass_AddrFunc(MOD_VBA6, FUNC_EBM)               'Get the address of EbMode in vba6.dll

        If i = 0 Then                                           'Found?
            i = Subclass_AddrFunc(MOD_VBA5, FUNC_EBM)             'VB5 perhaps, try vba5.dll
        End If

        Debug.Assert i                                          'Ensure the EbMode function was found
        Call Subclass_PatchRel(PATCH_01, i)                     'Patch the relative address to the EbMode api function
    End If

    Call Subclass_PatchRel(PATCH_03, Subclass_AddrFunc(MOD_USER, FUNC_SWL))     'Address of the SetWindowLong api function
    Call Subclass_PatchRel(PATCH_05, Subclass_AddrFunc(MOD_USER, FUNC_CWP))     'Address of the CallWindowProc api function
    '<EhFooter>
    Exit Sub
Subclass_Initialize_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_Initialize"
    Resume Next
    '</EhFooter>
End Sub

'Patch the machine code buffer offset with the relative address to the target address
Private Sub Subclass_PatchRel(ByVal nOffset As Long, _
                              ByVal nTargetAddr As Long)
    '<EhHeader>
    On Error GoTo Subclass_PatchRel_Err
    '</EhHeader>
    Call api_CopyMemory(ByVal (nAddrSubclass + nOffset), nTargetAddr - nAddrSubclass - nOffset - 4, 4)
    '<EhFooter>
    Exit Sub
Subclass_PatchRel_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_PatchRel"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Subclass_PatchTableA()
    '<EhHeader>
    On Error GoTo Subclass_PatchTableA_Err
    '</EhHeader>
    Const PATCH_07 As Long = 114
    Const PATCH_08 As Long = 130
    Call Subclass_PatchVal(PATCH_06, lngMsgCntA)
    Call Subclass_PatchVal(PATCH_07, Subclass_AddrMsgTbl(lngTableA1))
    Call Subclass_PatchVal(PATCH_08, Subclass_AddrMsgTbl(lngTableA2))
    '<EhFooter>
    Exit Sub
Subclass_PatchTableA_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_PatchTableA"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Subclass_PatchTableB()
    '<EhHeader>
    On Error GoTo Subclass_PatchTableB_Err
    '</EhHeader>
    Const PATCH_0A As Long = 145
    Const PATCH_0B As Long = 161
    Call Subclass_PatchVal(PATCH_09, lngMsgCntB)
    Call Subclass_PatchVal(PATCH_0A, Subclass_AddrMsgTbl(lngTableB1))
    Call Subclass_PatchVal(PATCH_0B, Subclass_AddrMsgTbl(lngTableB2))
    '<EhFooter>
    Exit Sub
Subclass_PatchTableB_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_PatchTableB"
    Resume Next
    '</EhFooter>
End Sub

'Patch the machine code buffer offset with the passed value
Private Sub Subclass_PatchVal(ByVal nOffset As Long, _
                              ByVal nValue As Long)
    '<EhHeader>
    On Error GoTo Subclass_PatchVal_Err
    '</EhHeader>
    Call api_CopyMemory(ByVal (nAddrSubclass + nOffset), nValue, 4)
    '<EhFooter>
    Exit Sub
Subclass_PatchVal_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_PatchVal"
    Resume Next
    '</EhFooter>
End Sub

'Worker function for InIDE - will only be called whilst running in the IDE
Private Function Subclass_SetTrue(bValue As Boolean) As Boolean
    '<EhHeader>
    On Error GoTo Subclass_SetTrue_Err
    '</EhHeader>
    Subclass_SetTrue = True
    bValue = True
    '<EhFooter>
    Exit Function
Subclass_SetTrue_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_SetTrue"
    Resume Next
    '</EhFooter>
End Function

'Set the window subclass
Private Function Subclass_Subclass(ByVal hWnd As Long) As Boolean
    '<EhHeader>
    On Error GoTo Subclass_Subclass_Err
    '</EhHeader>
    Const PATCH_02 As Long = 66                                'Address of the previous WndProc
    Const PATCH_04 As Long = 95                                'Address of the previous WndProc

    If hWndSub = 0 Then
        Debug.Assert api_IsWindow(hWnd)                         'Invalid window handle
        hWndSub = hWnd                                          'Store the window handle
        'Get the original window proc
        nAddrOriginal = api_GetWindowLong(hWnd, GWL_WNDPROC)
        Call Subclass_PatchVal(PATCH_02, nAddrOriginal)                  'Original WndProc address for CallWindowProc, call the original WndProc
        Call Subclass_PatchVal(PATCH_04, nAddrOriginal)                  'Original WndProc address for SetWindowLong, unsubclass on IDE stop
        'Set our WndProc in place of the original
        nAddrOriginal = api_SetWindowLong(hWnd, GWL_WNDPROC, nAddrSubclass)

        If nAddrOriginal <> 0 Then
            nAddrOriginal = 0
            Subclass_Subclass = True                                       'Success
        End If
    End If

    Debug.Assert Subclass_Subclass
    '<EhFooter>
    Exit Function
Subclass_Subclass_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_Subclass"
    Resume Next
    '</EhFooter>
End Function

'UnSubclass and release the allocated memory
Private Sub Subclass_Terminate()
    '<EhHeader>
    On Error GoTo Subclass_Terminate_Err
    '</EhHeader>
    Call Subclass_UnSubclass                                      'UnSubclass if the Subclass thunk is active
    Call api_GlobalFree(nAddrSubclass)                            'Release the allocated memory
    nAddrSubclass = 0
    ReDim lngTableA1(1 To 1)
    ReDim lngTableA2(1 To 1)
    ReDim lngTableB1(1 To 1)
    ReDim lngTableB2(1 To 1)
    '<EhFooter>
    Exit Sub
Subclass_Terminate_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_Terminate"
    Resume Next
    '</EhFooter>
End Sub

'Stop subclassing the window
Private Function Subclass_UnSubclass() As Boolean
    '<EhHeader>
    On Error GoTo Subclass_UnSubclass_Err

    '</EhHeader>
    If hWndSub <> 0 Then
        lngMsgCntA = 0
        lngMsgCntB = 0
        Call Subclass_PatchVal(PATCH_06, lngMsgCntA)                              'Patch the TableA entry count to ensure no further Proc callbacks
        Call Subclass_PatchVal(PATCH_09, lngMsgCntB)                              'Patch the TableB entry count to ensure no further Proc callbacks
        'Restore the original WndProc
        Call api_SetWindowLong(hWndSub, GWL_WNDPROC, nAddrOriginal)
        hWndSub = 0                                             'Indicate the subclasser is inactive
        Subclass_UnSubclass = True                              'Success
    End If

    '<EhFooter>
    Exit Function
Subclass_UnSubclass_Err:
    Controlar_Error Erl, Err.Description, "Reseter.modSocketMaster.Subclass_UnSubclass"
    Resume Next
    '</EhFooter>
End Function
