VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AuroNet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'<CSCC>
'--------------------------------------------------------------------------------
'    Componente  : AuroNet 0.1
'    Projecto    : Herramientas AuroWare
'
'    Descripción : Libreria de uso general para la interacción WEB
'    Depende de  : cSocketMaster, modSocketMaster, modBasico, AuroNetConf
'
'    Modificado  :
'   24/06/07 - UserAgent como propiedad
'   24/06/07 - Control de versión
'--------------------------------------------------------------------------------
'</CSCC>
'
Private WithEvents AuroSocket As CSocketMaster
Attribute AuroSocket.VB_VarHelpID = -1
Private ErrorGeneral As Boolean
Private ExitoGeneral As Boolean
Private MiTag As String
Private pBuffer As String
Private URL As String
Private Proxy As String
Private Proxy_Puerto As Integer
Private strHTTP As String

Public Function HTML_CONSULTAR() As Boolean
    '<EhHeader>
    On Error GoTo HTML_CONSULTAR_Err
    '</EhHeader>
    ExitoGeneral = False
    ErrorGeneral = False
    Debug.Print Time & "$ CONSULTAR - Inicio | " & URL
    strHTTP = vbNullString
    AuroSocket.Connect IIf(Proxy <> "", Proxy, HostDeURL) & pBuffer, IIf(Proxy <> "", Proxy_Puerto, 80)

    Do
        Esperar 0.5
    Loop Until AuroSocket.State = sckConnected Or AuroSocket.State = sckError Or ErrorGeneral

    If AuroSocket.State <> sckConnected Then
        HTML_CONSULTAR = False
        ExitoGeneral = False
        ErrorGeneral = True
    Else
        HTML_CONSULTAR = True
        ExitoGeneral = True
        ErrorGeneral = False
    End If

    AuroSocket.CloseSck
    Debug.Print Time & "$ CONSULTAR - Fin | " & HTML_CONSULTAR
    '<EhFooter>
    Exit Function
HTML_CONSULTAR_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.HTML_CONSULTAR"
    Resume Next
    '</EhFooter>
End Function

Public Function Exito()
    '<EhHeader>
    On Error GoTo Exito_Err
    '</EhHeader>
    Exito = ExitoGeneral
    '<EhFooter>
    Exit Function
Exito_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.Exito"
    Resume Next
    '</EhFooter>
End Function

Public Function HTML_GET(Optional Parametros As String) As String
    '<EhHeader>
    On Error GoTo HTML_GET_Err
    '</EhHeader>
Redireccion:
    Debug.Print "$ GET - Inicio | " & URL & " | " & Now
    AuroSocket.CloseSck

    If IsMissing(Parametros) Then pBuffer = vbNullString Else pBuffer = Parametros
    ErrorGeneral = False
    ExitoGeneral = False
    strHTTP = "GET " + IIf(Proxy <> "", URL, URLdeHost) & pBuffer + " HTTP/1.0" + vbCrLf
    strHTTP = strHTTP + "Accept: " + ACCEPT_TOKEN + vbCrLf
    strHTTP = strHTTP + "Referer: " + HostDeURL + vbCrLf
    strHTTP = strHTTP + "User-Agent: " + USERAGENT_TOKEN + vbCrLf
    strHTTP = strHTTP + "Host: " + HostDeURL + vbCrLf
    strHTTP = strHTTP + vbCrLf
    AuroSocket.Connect IIf(Proxy <> "", Proxy, HostDeURL) & pBuffer, IIf(Proxy <> "", Proxy_Puerto, 80)

    Do
        Esperar 0.5
    Loop Until ExitoGeneral Or ErrorGeneral

    AuroSocket.CloseSck
    Dim pRedireccion As Long
    Dim Redireccion As String
    Dim Cabeceras As String
    Cabeceras = ObtenerCabeceras(pBuffer)
    pRedireccion = InStr(1, Cabeceras, "Location:")

    If pRedireccion <> 0 Then
        pRedireccion = pRedireccion + Len("Location:")
        Redireccion = Trim$(Mid$(Cabeceras, pRedireccion, InStr(pRedireccion, Cabeceras, vbCrLf) - pRedireccion))

        If URL <> Redireccion Then
            Direccion = Redireccion
            Debug.Print Time & "$ GET - Redirección | " & URL
            GoTo Redireccion
        End If
    End If

    HTML_GET = ObtenerHTML(pBuffer)
    Debug.Print "$SOCKET HTML leido | " & Len(HTML_GET) & " | " & Now

    If Len(pBuffer) <> 0 And ExitoGeneral = True And ErrorGeneral = False Then
        ExitoGeneral = True
    Else
        ErrorGeneral = True
    End If

    '<EhFooter>
    Exit Function
HTML_GET_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.HTML_GET"
    Resume Next
    '</EhFooter>
End Function

Public Function IpLocal()
    '<EhHeader>
    On Error GoTo IpLocal_Err
    '</EhHeader>
    IpLocal = AuroSocket.LocalIP
    '<EhFooter>
    Exit Function
IpLocal_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.IpLocal"
    Resume Next
    '</EhFooter>
End Function

Private Function HostDeURL() As String
    '<EhHeader>
    On Error GoTo HostDeURL_Err
    '</EhHeader>
    HostDeURL = Replace$(Trim$(URL), "http://", vbNullString)
    Dim Init As Integer
    Init = InStr(1, HostDeURL, "/", vbTextCompare)

    If Init <> 0 Then HostDeURL = Left$(HostDeURL, Init - 1)
    '<EhFooter>
    Exit Function
HostDeURL_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.HostDeURL"
    Resume Next
    '</EhFooter>
End Function

Private Function URLdeHost() As String
    '<EhHeader>
    On Error GoTo URLdeHost_Err
    '</EhHeader>
    URLdeHost = Replace$(Trim$(URL), "http://", vbNullString)
    Dim Init As Integer
    Init = InStr(1, URLdeHost, "/", vbTextCompare)

    If Init <> 0 Then URLdeHost = Mid$(URLdeHost, Init)
    '<EhFooter>
    Exit Function
URLdeHost_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.URLdeHost"
    Resume Next
    '</EhFooter>
End Function

Private Sub AuroSocket_CloseSck()
    '<EhHeader>
    On Error GoTo AuroSocket_CloseSck_Err
    '</EhHeader>
    Debug.Print "$SOCKET - Cerrado" & " | " & Now
    ExitoGeneral = True
    '<EhFooter>
    Exit Sub
AuroSocket_CloseSck_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.AuroSocket_CloseSck"
    Resume Next
    '</EhFooter>
End Sub

Private Sub AuroSocket_Connect()
    '<EhHeader>
    On Error GoTo AuroSocket_Connect_Err
    '</EhHeader>
    AuroSocket.SendData strHTTP
    Debug.Print "$SOCKET - Datos enviados" & " | " & Now
    '<EhFooter>
    Exit Sub
AuroSocket_Connect_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.AuroSocket_Connect"
    Resume Next
    '</EhFooter>
End Sub

Private Sub AuroSocket_DataArrival(ByVal bytesTotal As Long)
    '<EhHeader>
    On Error GoTo AuroSocket_DataArrival_Err
    '</EhHeader>
    Dim Pedazo As String
    AuroSocket.GetData Pedazo
    pBuffer = pBuffer & Pedazo
    Debug.Print "$SOCKET - Respuesta recibida | " & AuroSocket.State & " | " & bytesTotal & " | " & Now
    '<EhFooter>
    Exit Sub
AuroSocket_DataArrival_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.AuroSocket_DataArrival"
    Resume Next
    '</EhFooter>
End Sub

Private Sub AuroSocket_Error(ByVal Number As Integer, _
                             Description As String, _
                             ByVal sCode As Long, _
                             ByVal Source As String, _
                             ByVal HelpFile As String, _
                             ByVal HelpContext As Long, _
                             CancelDisplay As Boolean)
    '<EhHeader>
    On Error GoTo AuroSocket_Error_Err
    '</EhHeader>
    Debug.Print "$SOCKET - ERROR!!!" & " | " & Now
    ErrorGeneral = True
    '<EhFooter>
    Exit Sub
AuroSocket_Error_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.AuroSocket_Error"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Class_Initialize()
    '<EhHeader>
    On Error GoTo Class_Initialize_Err
    '</EhHeader>
    USERAGENT_TOKEN = "Auronet " & AuroNetVer
    Set AuroSocket = New CSocketMaster
    '<EhFooter>
    Exit Sub
Class_Initialize_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.Class_Initialize"
    Resume Next
    '</EhFooter>
End Sub

Private Sub Class_Terminate()
    '<EhHeader>
    On Error Resume Next
    '</EhHeader>
    Set AuroSocket = Nothing
End Sub

Public Property Get Direccion() As Variant
    '<EhHeader>
    On Error GoTo Direccion_Err
    '</EhHeader>
    Direccion = URL
    '<EhFooter>
    Exit Property
Direccion_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.Direccion"
    Resume Next
    '</EhFooter>
End Property

Public Property Let Direccion(ByVal pDato As Variant)
    '<EhHeader>
    On Error GoTo Direccion_Err
    '</EhHeader>
    URL = pDato
    pDato = Replace$(pDato, "http://", "")

    If InStr(1, pDato, "/") = 0 Then URL = URL & "/"
    '<EhFooter>
    Exit Property
Direccion_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.Direccion"
    Resume Next
    '</EhFooter>
End Property

Public Property Get error() As Variant
    '<EhHeader>
    On Error GoTo error_Err
    '</EhHeader>
    error = ErrorGeneral
    '<EhFooter>
    Exit Property
error_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.error"
    Resume Next
    '</EhFooter>
End Property

Public Property Let error(ByVal Estado As Variant)
    '<EhHeader>
    On Error GoTo error_Err
    '</EhHeader>
    ErrorGeneral = Estado
    '<EhFooter>
    Exit Property
error_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.error"
    Resume Next
    '</EhFooter>
End Property

Public Property Get Tag() As Variant
    '<EhHeader>
    On Error GoTo Tag_Err
    '</EhHeader>
    Tag = MiTag
    '<EhFooter>
    Exit Property
Tag_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.Tag"
    Resume Next
    '</EhFooter>
End Property

Public Property Let Tag(ByVal Tag As Variant)
    '<EhHeader>
    On Error GoTo Tag_Err
    '</EhHeader>
    MiTag = Tag
    '<EhFooter>
    Exit Property
Tag_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.Tag"
    Resume Next
    '</EhFooter>
End Property

Public Property Get Agente() As Variant
    '<EhHeader>
    On Error GoTo Agente_Err
    '</EhHeader>
    Agente = USERAGENT_TOKEN
    '<EhFooter>
    Exit Property
Agente_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.Agente"
    Resume Next
    '</EhFooter>
End Property

Public Property Let Agente(ByVal vNewValue As Variant)
    '<EhHeader>
    On Error GoTo Agente_Err
    '</EhHeader>
    USERAGENT_TOKEN = CStr(vNewValue)
    '<EhFooter>
    Exit Property
Agente_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.Agente"
    Resume Next
    '</EhFooter>
End Property

Private Function ObtenerCabeceras(HTML As String) As String
    '<EhHeader>
    On Error GoTo ObtenerCabeceras_Err
    '</EhHeader>
    Dim Prueba As Long
    Prueba = InStr(1, HTML, vbCrLf & vbCrLf)

    If Prueba <> 0 Then ObtenerCabeceras = Left$(HTML, Prueba)
    '<EhFooter>
    Exit Function
ObtenerCabeceras_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.ObtenerCabeceras"
    Resume Next
    '</EhFooter>
End Function

Private Function ObtenerHTML(HTML As String) As String
    '<EhHeader>
    On Error GoTo ObtenerHTML_Err
    '</EhHeader>
    Dim Prueba As Long
    Prueba = InStr(1, HTML, vbCrLf & vbCrLf)

    If Prueba <> 0 Then ObtenerHTML = Mid$(HTML, Prueba + 4)
    '<EhFooter>
    Exit Function
ObtenerHTML_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.ObtenerHTML"
    Resume Next
    '</EhFooter>
End Function

Public Property Get Usar_Proxy() As Variant
    '<EhHeader>
    On Error GoTo Usar_Proxy_Err
    '</EhHeader>
    Usar_Proxy = Direccion
    '<EhFooter>
    Exit Property
Usar_Proxy_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.Usar_Proxy"
    Resume Next
    '</EhFooter>
End Property

Public Property Let Usar_Proxy(ByVal Direccion As Variant)
    '<EhHeader>
    On Error GoTo Usar_Proxy_Err
    '</EhHeader>
    Proxy = CStr(Direccion)
    '<EhFooter>
    Exit Property
Usar_Proxy_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.Usar_Proxy"
    Resume Next
    '</EhFooter>
End Property

Public Property Get Usar_Proxy_Puerto() As Variant
    '<EhHeader>
    On Error GoTo Usar_Proxy_Puerto_Err
    '</EhHeader>
    Usar_Proxy_Puerto = CStr(Proxy_Puerto)
    '<EhFooter>
    Exit Property
Usar_Proxy_Puerto_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.Usar_Proxy_Puerto"
    Resume Next
    '</EhFooter>
End Property

Public Property Let Usar_Proxy_Puerto(ByVal puerto As Variant)
    '<EhHeader>
    On Error GoTo Usar_Proxy_Puerto_Err
    '</EhHeader>
    Proxy_Puerto = CInt(puerto)
    '<EhFooter>
    Exit Property
Usar_Proxy_Puerto_Err:
    Controlar_Error Erl, Err.Description, "Reseter.AuroNet.Usar_Proxy_Puerto"
    Resume Next
    '</EhFooter>
End Property
